<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Custom extends CI_Model
{

    public function gudang($w = null, $order = null)
    {
        $this->db->select('gudang.*, user.name, user.username, user.gambar');
        $this->db->join('user', 'gudang.created_by = user.id');
        if ($w != null) {
            $this->db->where($w);
        }
        if ($order != null) {
            $this->db->order_by($order);
        }else{
            $this->db->order_by('id_gudang', 'desc');
        }
        
        $cek = $this->db->get('gudang')->result();
        foreach ($cek as $key => $value) {
            $img = $this->db->query("SELECT * FROM `gudang_gallery` WHERE gudang_id = $value->id_gudang")->row();
            $value->image = $img->image;
        }
        // echo "<pre>";
        // print_r($cek);
        // echo "</pre>";
        // die;
        return $cek;
    }

    public function shipping($w = null, $order = null)
    {
        $this->db->select('shiping.*, gudang.nama_brg, gudang.stok, gudang.kondisi');
        $this->db->join('gudang', 'shiping.gudang_id = gudang.id_gudang', 'left');
        if ($w != null) {
            $this->db->where($w);
        }
        if ($order != null) {
            $this->db->order_by($order);
        } else {
            $this->db->order_by('id_shiping', 'desc');
        }       
		$this->db->group_by('id_shiping');
        return $this->db->get('shiping');
    }

    public function aktivitas($w = null, $order = null)
    {
        $this->db->select('aktivitas.*, user.name, user.username, user.gambar');
        $this->db->join('user', 'aktivitas.pembuat = user.id');
        if ($w != null) {
            $this->db->where($w);
        }
        if ($order != null) {
            $this->db->order_by($order);
        } else {
            $this->db->order_by('aktivitas.tanggal', 'desc');
        } 

        $cek = $this->db->get('aktivitas')->result();
        $img = $this->db->query("SELECT * FROM `aktivitas_galeri` GROUP BY aktivitas_id")->result();
        foreach ($cek as $key => $value) {
            $img = $this->db->query("SELECT * FROM `aktivitas_galeri` WHERE aktivitas_id = $value->id_aktivitas")->row();
            $value->image = $img->image;
        }
       
        return $cek;
    }

    public function notif_aktivitas($w)
    {
        $this->db->select('aktivitas.*, user.name, user.username, user.gambar');
        $this->db->join('user', 'aktivitas.pembuat = user.id');
        if ($w != null) {
            $this->db->where($w);
        }      
        $this->db->order_by('id_aktivitas', 'desc');
      

        $cek = $this->db->get('aktivitas', 5)->result();
        $img = $this->db->query("SELECT * FROM `aktivitas_galeri` GROUP BY aktivitas_id")->result();
        foreach ($cek as $key => $value) {
            $img = $this->db->query("SELECT * FROM `aktivitas_galeri` WHERE aktivitas_id = $value->id_aktivitas")->row();
            $value->image = $img->image;
        }

        return $cek;
    }

    public function notif_shipping($w)
    {
        $this->db->select('shiping.*, gudang.nama_brg, gudang.stok, gudang.kondisi, user.name, user.username, user.gambar');
        $this->db->join('user', 'shiping.pengirim = user.id');
        $this->db->join('gudang', 'shiping.gudang_id = gudang.id_gudang', 'left');
    
        $this->db->where($w);
            
        $this->db->order_by('id_shiping', 'desc');
        return $this->db->get('shiping', 5);
    }

    public function notif($w)
    {
        if ($w == 0) {
            $notif = array(
                "aktivitas" => $this->notif_aktivitas(['status' => 0, 'aktivitas.is_deleted' => 0]),
                "shipping" => $this->notif_shipping(['status' => 'Dikirim', 'shiping.is_deleted' => 0])->result(),
            );
        }else{
            $notif = array(
                "aktivitas" => $this->notif_aktivitas(['is_seen' => 0, 'aktivitas.is_deleted' => 0]),
                "shipping" => $this->notif_shipping(['status' => 'Dikirim', 'shiping.is_deleted' => 0])->result(),
            );
        }
        
        return $notif;
    }

	public function getStok($nama)
	{
		$this->db->where('nama_brg', $nama);
		$data = $this->db->get('gudang')->result();
		$res['baik'] = 0;
		$res['rusak'] = 0;
		$res['expired'] = 0;
		foreach ($data as $key => $value) {
			if($value->kondisi == 'Baik'){
				$res['baik'] += $value->stok;
			}
			if($value->kondisi == 'Rusak'){
				$res['rusak'] += $value->stok;
			}
			if($value->kondisi == 'Expired'){
				$res['expired'] += $value->stok;
			}
			$res['berat'] = $value->berat;
			$res['kubikasi'] = $value->kubikasi;
		}
		return $res;
	}
}

/* End of file Custom.php */
