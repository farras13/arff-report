<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_model extends General {

	public function insData()
	{
		// Validasi Data
		if ($this->input->post('nama') == null && $this->input->post('namam') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan Nama Barang terisi!');
            redirect('Gudang', 'refresh');
        } 
        if ($this->input->post('jumlah') == null && $this->input->post('jumlahm') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan Jumlah sudah terisi!');
            redirect('Gudang', 'refresh');
        }
		if ($this->input->post('berat') == null && $this->input->post('beratm') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan Jumlah sudah terisi!');
            redirect('Gudang', 'refresh');
        }  
		if ($this->input->post('kubikasi') == null && $this->input->post('kubikasim') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan Jumlah sudah terisi!');
            redirect('Gudang', 'refresh');
        }  
        if ($this->input->post('kondisi') == null && $this->input->post('kondisim') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan Kondisi sudah terisi!');
            redirect('Gudang', 'refresh');
        }

		//initial Data
		$stok = 0;
		$kondisi = $this->input->post('kondisi') == null ?  $this->input->post('kondisim') :  $this->input->post('kondisi');
        $gudang = $this->m->getData('gudang', ['nama_brg' => $this->input->post('nama'), 'kondisi' => $kondisi])->row();
        $nama = $this->input->post('nama') == null ?  $this->input->post('namam') :  $this->input->post('nama');
        $jumlah = $this->input->post('jumlah') == null ?  $this->input->post('jumlahm') :  $this->input->post('jumlah');
        $berat = $this->input->post('berat') == null ?  $this->input->post('beratm') :  $this->input->post('berat');
        $kubikasi = $this->input->post('kubikasi') == null ?  $this->input->post('kubikasim') :  $this->input->post('kubikasi');

		if($gudang != null){
			$stok = $jumlah;
			$where = ['id_gudang' => $gudang->id_gudang, 'kondisi' => $kondisi];
			if($gudang->is_deleted != 0){
				$this->m->upd('gudang', ['stok' => $stok, 'berat' => $berat * $jumlah,'kubikasi' => $kubikasi * $jumlah, 'is_deleted' => 0], $where);                 
			}else{
				$stok = $gudang->stok + $jumlah;
				$this->m->upd('gudang', ['stok' => $stok], $where);
				// $stok_berat = $gudang->berat + ($berat * $jumlah);
				// $stok_kubikasi = $gudang->kubikasi + ($kubikasi * $jumlah);
				// $this->m->upd('gudang', ['stok' => $stok,'berat' => $stok_berat,'kubikasi' => $stok_kubikasi], $where);
			}
			$this->session->set_flashdata('toast', 'success:Succes Tambah Data!');
			redirect('Gudang', 'refresh');
	   }else{
			$gudang = $this->m->getData('gudang', ['nama_brg' => $this->input->post('nama')])->row();
		    if($gudang != null){
				$data = array(
					'id_gudang' => $gudang->id_gudang,
					'nama_brg' => $nama,
					'stok' => $jumlah,
					'berat' => $berat,
					'kubikasi' => $kubikasi,
					'kondisi' => $kondisi,
					'created_by' => $this->session->userdata('id_user'),
					'created' => date('Y-m-d'),
					'modified' => date('Y-m-d'),
				);

				$this->m->ins('gudang', $data);
				$last = $this->m->last_row('gudang', 'id_gudang')->id_gudang;
				$gambar = $this->m->getData('gudang_gallery', ['gudang_id' => $gudang->id_gudang])->result();
				
				foreach ($gambar as $key => $value) {
					$this->m->ins('gudang_gallery', ['gudang_id' => $last, 'image' => $value->image]);
				}
				$this->session->set_flashdata('toast', 'success:Succes Tambah Data!');
				redirect('Gudang', 'refresh');
		    }else{
				if($this->input->post('gambar') == null){
					$this->session->set_flashdata('toast', 'error: Gambar belum di isi!');            
					redirect('Gudang','refresh');            
				}else{
				 //    $stok_berat = $berat * $jumlah;
				 //    $stok_kubikasi = $kubikasi * $jumlah;
				 	$last_id = $this->m->last_row('gudang', 'id_gudang')->id_gudang;	
				 	$data = array(
						'id_gudang' => $last_id + 1 ,
						'nama_brg' => $nama,
						'stok' => $jumlah,
						'berat' => $berat,
						'kubikasi' => $kubikasi,
						'kondisi' => $kondisi,
						'created_by' => $this->session->userdata('id_user'),
						'created' => date('Y-m-d'),
						'modified' => date('Y-m-d'),
					);
			
					$this->m->ins('gudang', $data);
					$last = $this->m->last_row('gudang', 'id')->id;
					
					$gambar = explode("<>", $this->input->post('gambar'));
					for ($i = 0; $i < count($gambar); $i++) {
						if ($gambar[$i] != '' || $gambar[$i] != null) {
							$fg = str_replace(' ', '_', $gambar[$i]);
							$path = base_url('assets/upload/') . $fg;
							$this->m->ins('gudang_gallery', ['gudang_id' => $last, 'image' => $path]);
						}
					}
					$this->session->set_flashdata('toast', 'success:Succes Tambah Data!');
					redirect('Gudang', 'refresh');
				}
			}   
	   }
	}

	public function databygroup()
	{
		$this->db->where('is_deleted', 0);
		$this->db->group_by('nama_brg','kondisi');
		return $this->db->get('gudang');
	}
	
}

/* End of file Gudang_model.php */
