<?php

defined('BASEPATH') or exit('No direct script access allowed');

class General extends CI_Model
{
    public function getData($t, $w = null)
    {
        if ($w != null) {
            $this->db->where($w);
        }
        return $this->db->get($t);
    }

    public function last_row($t, $id)
    {
        $this->db->order_by($id, 'desc');
        return $this->db->get($t, 1)->row();
        
    }

    public function ins($t, $data)
    {
        return $this->db->insert($t, $data);
    }

    public function ins_id($t, $data)
    {
        return $this->db->insert_id($t, $data);
    }

    public function upd($t, $data, $w = null)
    {
        return $this->db->update($t, $data, $w);
    }

    public function del($t, $w = null)
    {
        if ($w != null) {
            return $this->db->delete($t, $w);
        } else {
            return $this->db->delete($t);
        }
    }
}
    
    /* End of file General.php */
