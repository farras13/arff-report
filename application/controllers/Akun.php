<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Akun extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('Login', 'refresh');
        }
        error_reporting(0);

        $this->load->model('General', 'm');
        $this->load->model('Custom', 'c');
        
    }
    
    public function index()
    {
       $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['role'] = $this->session->userdata('role');
        $data['data'] = $this->c->shipping(['shiping.is_deleted' => 0])->result();
        $data['gudang'] = $this->m->getData('gudang', ['gudang.is_deleted' => 0])->result();
        $data['notif_pegawai'] = $this->c->notif(0);
        $data['notif_admin'] = $this->c->notif(1);
        $this->load->view('template/header', $data);
        $this->load->view('akun', $data);
        $this->load->view('template/footer', $data);
    }
    
    public function detail_aktivitas($id)
    {
        $this->load->view('modal/aktivitas');   
    }

}

/* End of file Akun.php */

?>