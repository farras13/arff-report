<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
         if ($this->session->userdata('logged_in') != TRUE) {
            redirect('Login', 'refresh');
        }
        //Do your magic here
        $this->load->model('General', 'm');
        $this->load->model('Custom', 'c');
    }


    public function index()
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['data'] = $this->m->getData('user', ['is_deleted' => 0])->result();
        $data['notif_pegawai'] = $this->c->notif(0);
        $data['notif_admin'] = $this->c->notif(1);
        $this->load->view('template/header', $data);
        $this->load->view('user', $data);
        $this->load->view('template/footer', $data);
    }

    public function add_user()
    {
        $pass = $this->input->post('password');
        $name = $this->input->post('name');
        $user = $this->input->post('username');
        $role = $this->input->post('role');
        $nip = $this->input->post('nip');
        $data = array(
            'nip' => $nip,
            'name' => $name,
            'role' => $role,
            'username' => $user,
            'password' => md5($pass),
            'created' => date('Y-m-d')
        );
        $this->m->ins('user', $data);
        $this->session->set_flashdata('toast', 'success:Succes Tambah Data!');
        redirect('Setting', 'refresh');
    }

    public function upd_user($id)
    {
        $pass = $this->input->post('password');
        $name = $this->input->post('name');
        $user = $this->input->post('username');
        $role = $this->input->post('role');
        $nip = $this->input->post('nip');
        $cek = $this->m->getData('user', ['password' => md5($pass), 'id' => $id])->row();
        $data = array(
            'nip' => $nip,
            'name' => $name,
            'role' => $role,
            'username' => $user,
        );
        if ($cek == null) {
           $data['password'] = md5($pass);
        }

        $this->m->upd('user', $data, ['id' => $id]);
        $this->session->set_flashdata('toast', 'success:Succes Update Data!');
        redirect('Setting', 'refresh');
    }

    public function del_user($id)
    {
        $this->m->upd('user', ['is_active' => 0, 'is_deleted' => 1], ['id' => $id]);
        $this->session->set_flashdata('toast', 'success:Succes Delete Data!');
        redirect('Setting', 'refresh');
    }
}
    
    /* End of file Setting.php */
