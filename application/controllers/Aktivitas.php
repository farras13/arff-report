<?php

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Aktivitas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
         if ($this->session->userdata('logged_in') != TRUE) {
            redirect('Login', 'refresh');
        }
        //Do your magic here
        $this->load->model('General', 'm');
        $this->load->model('Custom', 'q');
        error_reporting(0);

    }

    public function index()
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['data'] = $this->q->aktivitas(['aktivitas.is_deleted' => 0]);
        $data['notif_pegawai'] = $this->q->notif(0);
        $data['notif_admin'] = $this->q->notif(1);
        $this->load->view('template/header', $data);
        $this->load->view('aktivitas', $data);
        $this->load->view('template/footer', $data);
    }

    public function ins_data()
    {
        if ($this->input->post('judul') == null && $this->input->post('judulm') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan judul sudah terisi!');
            redirect('Aktivitas', 'refresh');
        } 
        if ($this->input->post('tanggal') == null && $this->input->post('tanggalm') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan tanggal sudah terisi!');
            redirect('Aktivitas', 'refresh');
        } 
        if ($this->input->post('status') == null && $this->input->post('statusm') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan status sudah terisi!');
            redirect('Aktivitas', 'refresh');
        } 
        if ($this->input->post('deskripsi') == null && $this->input->post('deskripsim') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan deskripsi sudah terisi!');
            redirect('Aktivitas', 'refresh');
        } 
        if($this->input->post('gambar') == null){
            $this->session->set_flashdata('toast', 'error: Gambar belum di isi!');            
            redirect('Aktivitas','refresh');            
        }else{
            $judul = $this->input->post('judul') == null ? $this->input->post('judulm') : $this->input->post('judul');
            $tanggal = $this->input->post('tanggal') == null ? $this->input->post('tanggalm') : $this->input->post('tanggal');
            $status = $this->input->post('status') == null ? $this->input->post('statusm') : $this->input->post('status');
            $deskripsi = $this->input->post('deskripsi') == null ? $this->input->post('deskripsim') : $this->input->post('deskripsi');
            $data = array(
                'judul' => $judul,
                'tanggal' => $tanggal,
                'status' => $status,
                'deskripsi' => $deskripsi,
                'pembuat' => $this->session->userdata('id_user')
            );
    
            $this->m->ins('aktivitas', $data);
            
            $last = $this->m->last_row('aktivitas', 'id_aktivitas')->id_aktivitas;
            $gambar = explode("<>", $this->input->post('gambar'));
            for ($i = 0; $i < count($gambar); $i++) {
                if ($gambar[$i] != '' || $gambar[$i] != null) {
                     $fg = str_replace(' ', '_', $gambar[$i]);
                    $path = base_url('assets/upload/') . $fg;
                    $this->m->ins('aktivitas_galeri', ['aktivitas_id' => $last, 'image' => $path]);
                }
            }
    
            $this->session->set_flashdata('toast', 'success:Succes Tambah Data!');
            redirect('Aktivitas', 'refresh');
        }
    }

    public function upd_sts($id)
    {
        $data = array('status' => 1);
        $w = array('id_aktivitas' => $id);

        $this->m->upd('aktivitas', $data, $w);
        $this->session->set_flashdata('toast', 'success:Update Status Berhasil !');

        redirect('Aktivitas', 'refresh');
    }

    public function upd_status($id)
    {
        $data = array('status' => 1, 'is_seen' => 1);
        $w = array('id_aktivitas' => $id);

        $this->m->upd('aktivitas', $data, $w);
        $this->session->set_flashdata('toast', 'success:Update Status Berhasil !');

        redirect('Aktivitas', 'refresh');
    }

    public function add_foto()
    {
        if (!empty($_FILES)) {
            // File upload configuration 
            $uploadPath = './assets/upload/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = '*';

            // Load and initialize upload library 
            $this->load->library('upload');
            $this->upload->initialize($config);

            // Upload file to the server 
            if ($this->upload->do_upload('file')) {
                $fileData = $this->upload->data();
                return $fileData['file_name'];
            }
        }
    }

    public function del_data($id)
    {
        $this->m->upd('aktivitas', ['aktivitas.is_deleted' => 1], ['id_aktivitas' => $id]);
        $this->session->set_flashdata('toast', 'success:Delete Data Berhasil !');

        redirect('Aktivitas', 'refresh');
    }

    public function detail($id)
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['dokumentasi'] = $this->m->getData('aktivitas_galeri', ['aktivitas_id' => $id])->result();
        $data['data'] = $this->q->aktivitas(['aktivitas.id_aktivitas' => $id]);
        $this->load->view('template/header', $data);
        $this->load->view('detail_aktivitas', $data);
        $this->load->view('template/footer', $data);
    }
    
    public function print($id)
    {
        $data['gambar'] = $this->m->getData('aktivitas_galeri', ['aktivitas_id' => $id])->result();
        $data['data'] = $this->q->aktivitas(['aktivitas.id_aktivitas' => $id]);
        foreach ($data['data'] as $key => $value) {
            $data['day'] = date('d', strtotime($value->tanggal));
            $data['bln'] = date('m', strtotime($value->tanggal));
            $data['thn'] = date('Y', strtotime($value->tanggal));
            $coun = strlen($value->id_aktivitas);
            $b = $value->id_aktivitas;
            if ($coun == 1) {
                $ht = '000' . '' . $b;
            } elseif ($coun == 2) {
                $ht = '00' . '' . $b;
            } elseif ($coun == 3) {
                $ht = '0' . '' . $b;
            } else {
                $ht = $b;
            }
            $data['no'] = $ht;
        }
        $this->load->view('laporan/aktivitas', $data);
        
    }
}
    
    /* End of file Aktivitas.php */
