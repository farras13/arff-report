<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('general', 'm');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function doLogin()
    {
        $username = $this->input->post('username');
        $pass_temp = $this->input->post('pass');
        $pass = md5($pass_temp);

        $w = array('username' => $username, 'password' => $pass);
        $cek = $this->m->getData('user', $w)->row();

        if ($cek) {
            $array = array(
                'id_user' => $cek->id,
                'username' => $cek->username,
                'name' => $cek->name,
                'role' => $cek->role,
                'nip' => $cek->nip,
                'gambar' => $cek->gambar,
                'logged_in' => TRUE,
            );
            $this->session->set_userdata($array);
            $this->session->set_flashdata('toast', 'success:Welcome back Admin');
            redirect('Dashboard', 'refresh');
        } else {
            $this->session->set_flashdata('toast', 'error:Username or Password wrong!');
            redirect('Login', 'refresh');
        }
    }

    public function doLogout()
    {
        session_destroy();
        redirect('Login', 'refresh');
    }
}
    
    /* End of file Login.php */
