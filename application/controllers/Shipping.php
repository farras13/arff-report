<?php

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Shipping extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        clearstatcache(); 
		if ($this->session->userdata('logged_in') != TRUE) {
            redirect('Login', 'refresh');
        }
        //Do your magic here
        $this->load->model('General', 'm');
        $this->load->model('Gudang_model', 'gm');
        $this->load->model('Custom', 'c');
        //error_reporting(0);
    }

    public function index()
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['role'] = $this->session->userdata('role');
        $data['data'] = $this->c->shipping(['shiping.is_deleted' => 0])->result();
        $data['gudang'] = $this->m->getData('gudang', ['gudang.is_deleted' => 0])->result();
        $data['barang'] = $this->gm->databygroup()->result();
        $data['notif_pegawai'] = $this->c->notif(0);
        $data['notif_admin'] = $this->c->notif(1);
		foreach ($data['data'] as $key => $value) {
			$value->total_barang = $value->jumlah + $value->jumlah_rusak + $value->jumlah_expired;
			$value->total_berat = $value->total_barang * $value->berat;
			$value->total_kubikasi = $value->total_barang * $value->kubikasi;
		}
        $this->load->view('template/header', $data);
        $this->load->view('shipping', $data);
        $this->load->view('template/footer', $data);
    }

	public function getStok()
	{
		$nama = $this->input->post('nama');
		$data = $this->c->getStok($nama);
		echo json_encode($data);
	}

    public function ins_data_kirim()
    {
        if ($this->input->post('barang') == null || $this->input->post('jumlah') == null || $this->input->post('tujuan') == null|| $this->input->post('tgl_kirim') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan sudah terisi semua!');
            redirect('Shipping', 'refresh');
        } 
        $brg = $this->input->post('barang');
		$baik = 0; $rusak = 0; $expired = 0;
        $barang = $this->m->getData('gudang', ['nama_brg' => $brg])->result();
        // var_dump($this->input->post('tujuan'));
		foreach ($barang as $key => $value) {
			if ($value->kondisi == 'Baik') { $stok = $value->stok; $jumlah = $this->input->post('jumlah'); }
			if ($value->kondisi == 'Rusak') { $stok = $value->stok; $jumlah = $this->input->post('jumlah_rsk'); }
			if ($value->kondisi == 'Expired') { $stok = $value->stok; $jumlah = $this->input->post('jumlah_exp'); }
			if($stok < $jumlah){
				$this->session->set_flashdata('toast', 'error:Kuota barang melebihi yang ada digudang!');
            	redirect('Shipping', 'refresh');
			}else{
				$temp = $stok - $jumlah;
				$data = array('stok' => $temp);
				$this->m->upd('gudang', $data, ['id_gudang' => $value->id_gudang, 'kondisi' => $value->kondisi]);
			}
		}

		$data = array(
			'gudang_id' => $value->id_gudang,
			'awb' =>  $this->input->post('awb'),
			'nama' => $brg,
			'jumlah' => $this->input->post('jumlah') ,
			'jumlah_rusak' => $this->input->post('jumlah_rsk') ,
			'jumlah_expired' => $this->input->post('jumlah_exp') ,
			'berat' => $this->input->post('berat'),
			'kubikasi' =>  $this->input->post('kubikasi'),
			'service' =>  $this->input->post('service'),
			'tujuan' =>  $this->input->post('tujuan'),
			'tanggal_pengiriman' =>  $this->input->post('tgl_kirim'),
			'status' =>  "Dikirim",
			'type' =>  0,
			'pengirim' => $this->input->post('pengirim'),
			'user_id' => $this->session->userdata('id_user'),
		);
		$this->m->ins('shiping', $data);
        redirect('Shipping', 'refresh');
    }

	public function ins_data_terima()
    {
        if ($this->input->post('barang') == null || $this->input->post('jumlah') == null || $this->input->post('tujuan') == null|| $this->input->post('tgl_kirim') == null) {
            $this->session->set_flashdata('toast', 'error:Pastikan sudah terisi semua!');
            redirect('Shipping', 'refresh');
        } 
        $brg = $this->input->post('barang');
		$baik = 0; $rusak = 0; $expired = 0;
        $barang = $this->m->getData('gudang', ['nama_brg' => $brg])->result();
        // var_dump($this->input->post('tujuan'));
		// foreach ($barang as $key => $value) {
		// 	if ($value->kondisi == 'Baik') { $stok = $value->stok; $jumlah = $this->input->post('jumlah'); }
		// 	if ($value->kondisi == 'Rusak') { $stok = $value->stok; $jumlah = $this->input->post('jumlah_rsk'); }
		// 	if ($value->kondisi == 'Expired') { $stok = $value->stok; $jumlah = $this->input->post('jumlah_exp'); }
		// 	if($stok < $jumlah){
		// 		$this->session->set_flashdata('toast', 'error:Kuota barang melebihi yang ada digudang!');
        //     	redirect('Shipping', 'refresh');
		// 	}else{
		// 		$temp = $stok-$jumlah;
		// 		$data = array('stok' => $temp);
		// 		$this->m->upd('gudang', $data, ['id_gudang' => $value->id_gudang]);
		// 	}
		// }
	
		$data = array(
			'gudang_id' => $barang[0]->id_gudang,
			'awb' =>  $this->input->post('awb'),
			'nama' => $brg,
			'jumlah' => $this->input->post('jumlah') ,
			'jumlah_rusak' => $this->input->post('jumlah_rsk') ,
			'jumlah_expired' => $this->input->post('jumlah_exp') ,
			'berat' => $this->input->post('berat'),
			'kubikasi' =>  $this->input->post('kubikasi'),
			'service' =>  $this->input->post('service'),
			'tujuan' =>  $this->input->post('tujuan'),
			'tanggal_pengiriman' =>  $this->input->post('tgl_kirim'),
			'status' =>  "Dikirim",
			'type' =>  1,
			'pengirim' => $this->input->post('pengirim'),
			'user_id' => $this->session->userdata('id_user'),
		);
		$this->m->ins('shiping', $data);

        redirect('Shipping', 'refresh');
    }

    public function upd_status($id)
    {
        $cek = $this->m->getData('shiping', ['id_shiping' => $id])->row();
        $gudang = $this->m->getData('gudang', ['id_gudang' => $cek->gudang_id])->result();
		foreach ($gudang as $key => $value) {
			if($cek->type == 1){
				if($value->kondisi == 'Baik'){
					$stok = $value->stok + $cek->jumlah;
				}
				if($value->kondisi == 'Rusak'){
					$stok = $value->stok + $cek->jumlah_rusak;
				}
				if($value->kondisi == 'expired'){
					$stok = $value->stok + $cek->jumlah_expired;
				}
				$this->m->upd('gudang', ['stok' => $stok, 'is_deleted' => 0], ['id_gudang' => $value->id]);
			}
		}
        $data = array('tanggal_diterima' => date('Y-m-d'), 'status' => 'Diterima');
        $w = array('id_shiping' => $id,);
        $this->m->upd('shiping', $data, $w);

        $this->session->set_flashdata('toast', 'success:Succes Ubah Data!');
        redirect('Shipping', 'refresh');
    }

    public function del_data($id)
    {
		$data= $this->m->getData('shiping', ['id_shiping' => $id])->row();
		$gudang = $this->m->getData('gudang', ['id_gudang' => $data->gudang_id])->result();
		foreach ($gudang as $key => $value) {
			if($data->type == 0){
				if($value->kondisi == "Baik"){
					$stok = $value->stok + $data->jumlah;
					$w = ['kondisi' => 'Baik', 'id_gudang' => $data->gudang_id];
					$res = ['stok' => $stok];
				}
				if($value->kondisi == "Rusak"){
					$stok = $value->stok + $data->jumlah_rusak;
					$w = ['kondisi' => 'Rusak', 'id_gudang' => $data->gudang_id];
					$res = ['stok' => $stok];
				}
				if($value->kondisi == "Expired"){
					$stok = $value->stok + $data->jumlah_expired;
					$w = ['kondisi' => 'Expired', 'id_gudang' => $data->gudang_id];
					$res = ['stok' => $stok];
				}				
				$this->m->upd('gudang', $res, $w);
			}else{
				if($data->status != 'Dikirim'){
					if($value->kondisi == "Baik"){
						$stok = $value->stok - $data->jumlah;
						$w = ['kondisi' => 'Baik', 'id_gudang' => $data->gudang_id];
						$res = ['stok' => $stok];
					}
					if($value->kondisi == "Rusak"){
						$stok = $value->stok - $data->jumlah_rusak;
						$w = ['kondisi' => 'Rusak', 'id_gudang' => $data->gudang_id];
						$res = ['stok' => $stok];
					}
					if($value->kondisi == "Expired"){
						$stok = $value->stok - $data->jumlah_expired;
						$w = ['kondisi' => 'Expired', 'id_gudang' => $data->gudang_id];
						$res = ['stok' => $stok];
					}
					$this->m->upd('gudang', $res, $w);
				}
			}
		}	
        // $this->m->upd('shiping', ['shiping.is_deleted' => 1], ['id_shiping' => $id]);
		$this->m->del('shiping',['id_shiping' => $id]);
        $this->session->set_flashdata('toast', 'success:Delete Data Berhasil !');
        redirect('Shipping', 'refresh');
    }

    public function detail($id)
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['data'] = $this->c->shipping(['shiping.is_deleted' => 0, 'id_shiping' => $id])->result();
        $d = $this->c->shipping(['shiping.is_deleted' => 0, 'id_shiping' => $id])->row();
        $data['dokumentasi'] = $this->m->getData('gudang_gallery', ['gudang_id' => $d->gudang_id])->result();
        $this->load->view('template/header', $data);
        $this->load->view('detail_shiping', $data);
        $this->load->view('template/footer', $data);
    }

    public function print($id)
    {
        $data['data'] = $this->c->shipping(['shiping.is_deleted' => 0, 'id_shiping' => $id])->row();
        if ($data['data']->tanggal_diterima != 0000 - 00 - 00) {
            $data['day'] = date('d', strtotime($data['data']->tanggal_diterima));
            $data['bln'] = date('m', strtotime($data['data']->tanggal_diterima));
            $data['thn'] = date('Y', strtotime($data['data']->tanggal_diterima));
        } else {
            $data['day'] = date('d', strtotime($data['data']->tanggal_pengiriman));
            $data['bln'] = date('m', strtotime($data['data']->tanggal_pengiriman));
            $data['thn'] = date('Y', strtotime($data['data']->tanggal_pengiriman));
        }
        $coun = strlen($data['data']->id_shiping);
        $b = $data['data']->id_shiping;
        if ($coun == 1) {
            $ht = '000' . '' . $b;
        } elseif ($coun == 2) {
            $ht = '00' . '' . $b;
        } elseif ($coun == 3) {
            $ht = '0' . '' . $b;
        } else {
            $ht = $b;
        }
        $data['no'] = $ht;
        $data['gambar'] = $this->m->getData('gudang_gallery', ['gudang_id' => $data['data']->gudang_id])->result();
        $this->load->view('laporan/shiping', $data);
    }
}

/* End of file Shipping.php */
