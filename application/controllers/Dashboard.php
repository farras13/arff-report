<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('Login', 'refresh');
        }
        error_reporting(0);

        $this->load->model('General', 'm');
        $this->load->model('Custom', 'c');
        
    }
    
    public function index()
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['aktivitas'] = $this->c->aktivitas(['aktivitas.is_deleted' => 0]);
        $data['galeri'] = $this->m->getData('aktivitas_galeri')->result();
        $data['gudang'] = $this->c->gudang(['gudang.is_deleted' => 0]);
        $data['ship'] = $this->c->shipping(['shiping.is_deleted' => 0])->result();
        $data['tgl_produk'] = $this->db->query("SELECT * FROM `gudang`ORDER BY modified DESC LIMIT 1")->row();
        $data['tgl_produk_status'] = $this->db->query("SELECT * FROM `gudang`ORDER BY modified_status DESC LIMIT 1")->row();
        $data['notif_pegawai'] = $this->c->notif(0);
        $data['notif_admin'] = $this->c->notif(1);

		foreach ($data['gudang'] as $key => $value) {
			# code...
		}

        $this->load->view('template/header', $data);
        $this->load->view('index', $data);
        $this->load->view('template/footer', $data);
    }
    
    public function detail_aktivitas($id)
    {
        $this->load->view('modal/aktivitas');   
    }

}

/* End of file Dashboard.php */

?>
