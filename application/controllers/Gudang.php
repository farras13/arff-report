<?php

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
         if ($this->session->userdata('logged_in') != TRUE) {
            redirect('Login', 'refresh');
        }
        //Do your magic here
        $this->load->model('General', 'm');
        $this->load->model('Custom', 'c');
        $this->load->model('Gudang_model', 'gm');
        // error_reporting(0);

    }

    public function index()
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['data'] = $this->c->gudang(['gudang.is_deleted' => 0]);
        $data['notif_pegawai'] = $this->c->notif(0);
        $data['notif_admin'] = $this->c->notif(1);
		foreach ($data['data'] as $key => $value) {
			$value->berat = $value->berat * $value->stok;
			$value->kubikasi = $value->kubikasi * $value->stok;
		}
        $this->load->view('template/header', $data);
        $this->load->view('storage', $data);
        $this->load->view('template/footer', $data);

    }

    public function ins_data()
    {
		$this->gm->insData();
    }

	public function getstok()
	{
		$nama = $this->input->post('name');
		$data = $this->m->getData('gudang', ['nama_brg' => $nama])->row();
		echo json_encode($data);
	}

    public function add_foto()
    {
        if (!empty($_FILES)) {
            // File upload configuration 
            $uploadPath = './assets/upload/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = '*';

            // Load and initialize upload library 
            $this->load->library('upload');
            $this->upload->initialize($config);

            // Upload file to the server 
            if ($this->upload->do_upload('file')) {
                $fileData = $this->upload->data();
                return $fileData['file_name'];
            }
        }
    }

    public function upd_status($id, $sts)
    {
        $w = array('id_gudang' => $id);
        $d = array('kondisi' => $sts, 'modified_status' => date('Y-m-d'));
        $this->m->upd('gudang', $d, $w);
        $this->session->set_flashdata('toast', 'success:Succes Ubah Data!');
        redirect('Gudang', 'refresh');
    }

	public function upd_data()
    {
		$id= $this->input->post('id');
		$jumlah= $this->input->post('jumlah');
		
        $w = array('id' => $id);
        $d = array('stok' => $jumlah, 'modified_status' => date('Y-m-d'));
        $this->m->upd('gudang', $d, $w);
        $this->session->set_flashdata('toast', 'success:Succes Ubah Data!');
        redirect('Gudang', 'refresh');
    }

    public function del_data($id)
    {
        $this->m->upd('gudang', ['gudang.is_deleted' => 1, 'stok' => 0], ['id_gudang' => $id]);
        $this->session->set_flashdata('toast', 'success:Delete Data Berhasil !');

        redirect('Gudang', 'refresh');
    }
    
    public function detail($id)
    {
        $data['url'] = $this->uri->segment(1);
        $data['nama'] = $this->session->userdata('name');
        $data['id_akun'] = $this->session->userdata('id_user');
        $data['nip'] = $this->session->userdata('nip');
        $data['gambar'] = $this->session->userdata('gambar');
        $data['data'] = $this->c->gudang(['gudang.is_deleted' => 0, 'id_gudang' => $id]);
        $data['dokumentasi'] = $this->m->getData('gudang_gallery', ['gudang_id' => $id])->result();
        $this->load->view('template/header', $data);
        $this->load->view('detail_gudang', $data);
        $this->load->view('template/footer', $data);
    }
    
    public function print($id)
    {
        $data['gambar'] = $this->m->getData('gudang_gallery', ['gudang_id' => $id])->result();
        $data['data'] = $this->c->gudang(['gudang.is_deleted' => 0, 'id_gudang' => $id]);
        foreach ($data['data'] as $key => $value) {
            $data['day'] = date('d', strtotime($value->created));
            $data['bln'] = date('m', strtotime($value->created));
            $data['thn'] = date('Y', strtotime($value->created));
            $coun = strlen($value->id_gudang);
            $b = $value->id_gudang;
            if ($coun == 1) {
                $ht = '000' . '' . $b;
            } elseif ($coun == 2) {
                $ht = '00' . '' . $b;
            } elseif ($coun == 3) {
                $ht = '0' . '' . $b;
            } else {
                $ht = $b;
            }
            $data['no'] = $ht;
        }
        $this->load->view('laporan/storage', $data);
        
    }
}
    
    /* End of file Gudang.php */
