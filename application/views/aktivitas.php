<div class="col py-3 mx-3">
    <h3 class="d-none d-md-block">Aktivitas</h3>
	<?php if($this->session->userdata('item') != 3): ?>
    <div class="p-4 card-aktifitas">

        <form class="form-aktivitas" action="<?= base_url('aktivitas/ins_data'); ?>" method="post" enctype="multipart/form-data" onsubmit="kirimform()" novalidate>
            <div class="bg-white rounded-form p-2">
                <div class="dropzone" id="foto">

                </div>
                <input type="hidden" id="gambar" name="gambar">
            </div>
            <div class="card border-0 rounded-form p-2">

                <div class="card-body">
                    <h4>Tambah Kegiatan</h4>
                    <div class="row g-2">
                        <div class="col-4">
                            <p class="m-0">Judul Kegiatan</p>
                        </div>
                        <div class="col-8">
                            <input type="text" name="judul" id="judul" placeholder="Input Judul Kegiatan" />
                        </div>
                        <div class="col-4">
                            <p class="m-0">Tanggal</p>
                        </div>
                        <div class="col-8">
                            <input type="date" name="tanggal" id="tanggal" value="<?= date('Y-m-d'); ?>" placeholder="Input Judul Kegiatan" />
                        </div>
                        <div class="col-4">
                            <p class="m-0">Status</p>
                        </div>
                        <div class="col-8">
                            <select name="status" id="status" required>
                                <option value="">Pilih Status</option>
                                <option value="1">SELESAI</option>
                                <option value="0">BELUM SELESAI</option>
                            </select>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Deskripsi Kegiatan</p>
                        </div>
                        <div class="col-8">
                            <textarea name="deskripsi" id="deskripsi" cols="50" rows="10" placeholder="Input Deskripsi Kegiatan"></textarea>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-8">
                            <button type="submit">Submit</button>
                        </div>
                    </div>
                </div>
                <div class="d-sm-block d-md-none row mt-4">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input placeholder="Masukan Judul Kegiatan" name="judulm" type="text" class="form-control" id="nama_barang">
                            <label for="nama_barang">Judul Kegiatan</label>
                        </div>
                        <div class="form-group">
                            <input type="date" class="form-control" name="tanggalm" id="nama_barang">
                            <label for="nama_barang">Tanggal</label>
                        </div>
                        <div class="form-group">
                            <select name="statusm" id="">
                                <option value="">Pilih Status</option>
                                <option value="1">SELESAI</option>
                                <option value="0">BELUM SELESAI</option>
                            </select>
                        </div>
                        <div class="form-group mt-3">
                            <input placeholder="Masukan Deskripsi Kegiatan" type="text" name="deskripsim" class="form-control" id="nama_barang">
                            <label for="nama_barang">Deskripsi Kegiatan</label>
                        </div>
                    </div>
                </div>
                <button class="d-sm-block d-md-none mt-3" type="submit">Submit</button>
            </div>
        </form>

    </div>
    <?php endif; ?>
	<div class="mt-4 table-responsive">
        <table id="table_id" class="display table">
            <thead>
                <tr>
                    <th>Judul</th>
                    <th style="text-align: center;">Hari/Tanggal</th>
                    <th style="text-align: center;">Pengunggah</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">Konfirmasi Penanggung Jawab</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody style="vertical-align: middle;">
                <?php foreach ($data as $key => $value) { ?>
                    <tr>
                        <td><img src="<?= $value->image ?>" alt="" style="margin-right: 35px;" width="50px" height="50px"><?= $value->judul; ?></td>
                        <td style="text-align: center;"><?= date('d F Y', strtotime($value->tanggal)); ?></td>
                        <td><center><?= $value->name; ?></center></td>
                        <td>
                            <center>
                                <?php if ($value->status == 0) {
                                    echo '<img class="me-2" src="./assets/images/icon_close.png" alt=""><b>Belum Selesai</b>';
                                } else {
                                    echo '<img class="me-2" src="./assets/images/icon_check.png" alt=""><b>Selesai</b>';
                                } ?>
                            </center>    
                        </td>
                        <td>
                            <center>
                                <?php if ($value->is_seen == 0) {
                                        echo '<img class="me-2" src="./assets/images/icon_close.png" alt=""><b>Belum Diperiksa</b>';
                                } else {
                                    echo '<img class="me-2" src="./assets/images/icon_check.png" alt=""><b>Sudah Diperiksa</b>';
                                } ?>
                            </center>    
                        </td>
                            
                        <td>
                            <center>
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img width="35px" src="./assets/images/dot_actions.png" alt="" srcset="">
                                </button>
                                <ul class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton1">
                                    <a href="<?= base_url('Aktivitas/detail/') . $value->id_aktivitas; ?>" target="_blank">
                                        <li class="dropdwon-item">Detail</li>
                                    </a>
                                    <hr>
                                    <a href="<?= base_url('Aktivitas/print/') . $value->id_aktivitas; ?>" target="_blank">
                                        <li class="dropdwon-item">Print</li>
                                    </a>
                                    <hr>
                                    <a href="<?= base_url('Aktivitas/print/') . $value->id_aktivitas; ?>" target="_blank">
                                        <li class="dropdwon-item">Download PDF</li>
                                    </a>
                                    
                                    <?php if ($this->session->userdata('role') == 3 AND $value->status == 0) {  ?>
                                    <hr>
                                        <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Aktivitas/upd_sts/') . $value->id_aktivitas ?>">Konfirmasi Selesai</a>
                                        </li>
                                    <?php }  if ( $value->is_seen == 0 AND $this->session->userdata('role') != 3 ) { ?>
                                    <hr>
                                        <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Aktivitas/upd_status/') . $value->id_aktivitas ?>">Konfirmasi DIPERIKSA</a>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->session->userdata('role') != 3) {  ?>
                                        <hr>
                                        <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Aktivitas/del_data/') . $value->id_aktivitas ?>">Delete</a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            </center>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
