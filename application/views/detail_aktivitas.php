<div class="col py-3 mx-3">
    <h3>Detail Aktivitas</h3>
    <div class="p-4 card-aktifitas" style="margin:10px;">
        <div class="bg-white rounded-form p-2">
             <?php foreach ($data as $d) : ?>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>JUDUL KEGIATAN</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->judul; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>HARI TANGGAL</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= date('d F Y', strtotime($d->tanggal)); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>STATUS</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?php if ($d->status == 0) {
                            echo '<img class="me-2" src="./assets/images/icon_close.png" alt=""><b>Belum Selesai</b>';
                        } else {
                            echo '<img class="me-2" src="./assets/images/icon_check.png" alt=""><b>Selesai</b>';
                        } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>KONFRIMASI KANIT</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?php if ($d->is_seen == 0) {
                            echo '<img class="me-2" src="./assets/images/icon_close.png" alt=""><b>Belum Diperiksa</b>';
                        } else {
                            echo '<img class="me-2" src="./assets/images/icon_check.png" alt=""><b>Sudah Diperiksa</b>';
                        } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>KETERANGAN</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-12">
                        <textarea name="" id="" cols="30" rows="10" style="text-align:left" disabled>
                            <?= $d->deskripsi; ?>
                        </textarea>
                    </div>
                </div>
            <?php endforeach; ?>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>DOKUMENTASI</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-12" style="padding:10px;">
                        <?php foreach ($dokumentasi as $g) : ?>
                            <img src="<?= $g->image; ?>" alt="dokumentasi barang" style="width:500px">
                        <?php endforeach; ?>
                    </div>
                </div>  
        </div>



    </div>

</div>
</div>
</div>
