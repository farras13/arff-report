<div class="col py-3 mx-3 overflow-hidden">
    <h3 class="d-none d-md-block">Dashboard</h3>
    <div>
        <p class="m-0 mt-4 text-md-start text-center">Aktivitas</p>
        <div class="bg-card">
            <div class="main-carousel">
                <?php foreach ($aktivitas as $a) { ?>
                    <div class="carousel-cell" data-bs-toggle="modal" data-bs-target="#exampleModal<?= $a->id_aktivitas; ?>">
                        <div class="card card-aktivitas">
                            <div class="card-body">
                                <h5 class="card-title"> <?= $a->judul; ?></h5>
                                <p class="card-text"><?= date('l', strtotime($a->tanggal)); ?>,<?= date('d F Y', strtotime($a->tanggal)); ?></p>
                                <div class="text-center">
                                    <img width="180px" height="180px" src="<?= $a->image; ?>" alt="">
                                </div>
                                <p class="card-text mt-2">
                                    <?php if (strlen($a->deskripsi) > 50) {
                                        echo substr($a->deskripsi, 0, 50) . '...';
                                    } else {
                                        echo $a->deskripsi;
                                    }  ?>
                                </p>
                                <div class="d-flex justify-content-center" style="gap: 10px;">
                                    <div class="d-flex flex-row align-items-center">
                                        <?php if ($a->status == 0) { ?>
                                            <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">
                                        <?php } else { ?>
                                            <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                        <?php } ?>
                                        <p class="m-0 fw-bold status-aktivitas"><?php if ($a->status == 0) {
                                                                                    echo "BELUM SELESAI";
                                                                                } else {
                                                                                    echo "SELESAI";
                                                                                } ?></p>
                                    </div>
                                    <div class="d-flex flex-row align-items-center">
                                        <?php if ($a->is_seen == 0) { ?>
                                            <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">
                                        <?php } else { ?>
                                            <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                        <?php } ?>
                                        <p class="m-0 fw-bold status-aktivitas"> <?php if ($a->is_seen == 0) {
                                                                                        echo "BELUM DIPERIKSA";
                                                                                    } else {
                                                                                        echo "SUDAH DIPERIKSA";
                                                                                    } ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <a href="<?= base_url("Aktivitas"); ?>" class="btn btn-tambah d-sm-block d-md-none mt-4">+ Tambah Kegiatan</a>
        </div>
    </div>
    <div>
        <p class="m-0 mt-4 text-md-start text-center">Gudang</p>
        <div class="bg-card">
            <div class="row row-cols-3 align-items-start">
                <?php foreach ($gudang as $g) : ?>
                    <?php if ($g->kondisi == "Baik") : ?>
                        <div class="col mb-2">
                            <div class="d-flex">
                                <div class="pill-baik me-2">
                                    <p class="m-0 mx-auto fw-bold"><?= $g->stok; ?></p>
                                </div>
                                <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                            </div>
                        </div>
                    <?php elseif ($g->kondisi == "Kosong") : ?>
                        <div class="col mb-2">
                            <div class="d-flex">
                                <div class="pill-kosong me-2">
                                    <p class="m-0 mx-auto fw-bold"><?= $g->stok; ?></p>
                                </div>
                                <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="col mb-2">
                            <div class="d-flex">
                                <div class="pill-rusak me-2">
                                    <p class="m-0 mx-auto fw-bold"><?= $g->stok; ?></p>
                                </div>
                                <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <a href="<?= base_url('Gudang'); ?>" class="btn btn-tambah d-sm-block d-md-none mt-4">+ Tambah Barang</a>
            <p class="last-updated d-none d-md-block">Terakhir diupdate : <?= date('d F Y', strtotime($tgl_produk->modified)); ?></p>
        </div>
    </div>
    <div class="d-none d-md-block">
        <p class="m-0 mt-4 text-md-start text-center">Status Barang</p>
        <div class="bg-card">
            <div class="row row-cols-3 align-items-start">
                <?php foreach ($gudang as $g) : ?>
                    <?php if ($g->kondisi == "Baik") : ?>
                        <div class="col mb-2">
                            <div class="d-flex">
                                <div class="pill-baik me-2">
                                    <p class="m-0 mx-auto fw-bold">BAIK</p>
                                </div>
                                <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                            </div>
                        </div>
                    <?php elseif ($g->kondisi == "Expired") : ?>
                        <div class="col mb-2">
                            <div class="d-flex">
                                <div class="pill-kosong me-2">
                                    <p class="m-0 mx-auto fw-bold">Expired</p>
                                </div>
                                <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="col mb-2">
                            <div class="d-flex">
                                <div class="pill-rusak me-2">
                                    <p class="m-0 mx-auto fw-bold">RUSAK</p>
                                </div>
                                <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <p class="last-updated">Terakhir diupdate : <?= date('d F Y', strtotime($tgl_produk_status->modified_status)); ?></p>
        </div>
    </div>
    <div>
        <p class="m-0 mt-4 text-md-start text-center">Pengiriman</p>
        <div class="bg-card w-100">
            <div class="kontainer-pengiriman overflow-auto">
                <?php $no = 1;
                foreach ($ship as $s) : ?>
                    <div class="card-pengiriman">
                        <div class="d-flex flex-column justify-content-center align-items-center">
							<div class="d-flex flex-column text-center">
								<p class="m-0 fw-bold"><?= $s->type == 1 ? "Barang Masuk" : "Barang Keluar"; ?></p>
							</div>
							<div class="d-flex flex-row my-2 my-md-4">
								<p class="m-0 fw-bold me-3" style="font-size: 14px;"><?= $s->pengirim == 'own' ? 'Rajawali Perkasa Logistic' : ($s->pengirim == 'OWN' ? 'Rajawali Perkasa Logistic' : $s->pengirim)  ?></p>
								<img src="<?= base_url(); ?>/assets/images/icon_arrow.png" alt="" style="width: 20px; height: 20px;">
								<p class="m-0 fw-bold ms-3" style="font-size: 14px;"><?= $s->tujuan == 'own' ? 'Rajawali Perkasa Logistic' : ($s->tujuan == 'OWN' ? 'Rajawali Perkasa Logistic' : $s->tujuan)?></p>
							</div>                           
                            <div class="d-flex flex-column text-center my-2 my-md-4">
                                <p class="m-0"><?= $s->nama_brg; ?></p>
                                <p class="m-0"><?= $s->jumlah; ?> Unit</p>
                            </div>
                            <?php if ($s->status == "Dikirim") : ?>
                                <div class="status-dikirim">
                                    <p class="m-0">DIKIRIM</p>
                                </div>
                            <?php else : ?>
                                <div class="status-diterima">
                                    <p class="m-0">DITERIMA</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php $no++;
                    if ($no > 5) {
                        break;
                    }
                endforeach; ?>
            </div>
            <a href="<?= base_url('Shipping'); ?>" class="btn btn-tambah d-sm-block d-md-none mt-4">+ Tambah Pengiriman</a>
        </div>
    </div>
    <!-- Modal -->
    <?php foreach ($aktivitas as $a) { ?>
        <div class="modal jojo" id="exampleModal<?= $a->id_aktivitas; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="d-flex justify-content-center" style="gap: 10px;">
                            <div class="d-flex flex-row align-items-center">
                                <?php if ($a->status == 0) { ?>
                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">
                                <?php } else { ?>
                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                <?php } ?>
                                <p class="m-0 fw-bold"><?php if ($a->status == 0) {
                                                            echo "BELUM SELESAI";
                                                        } else {
                                                            echo "SELESAI";
                                                        } ?></p>
                            </div>
                            <div class="d-flex flex-row align-items-center">
                                <?php if ($a->is_seen == 0) { ?>
                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">
                                <?php } else { ?>
                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                <?php } ?>

                                <p class="m-0 fw-bold"><?php if ($a->is_seen == 0) {
                                                            echo "BELUM DIPERIKSA";
                                                        } else {
                                                            echo "SUDAH DIPERIKSA";
                                                        } ?></p>
                            </div>
                        </div>

                        <!-- Flickity HTML init -->
                        <div class="gallery js-flickity mt-4" data-flickity-options='{  "percentPosition": false, "pageDots": false,  "adaptiveHeight": true,"wrapAround": true }'>
                            <?php foreach ($galeri as $g) : if ($g->aktivitas_id == $a->id_aktivitas) : ?>
                                    <div class="carousel-cell">
                                        <img class="me-2" src="<?= $g->image; ?>" alt="" width="210px" height="210px"> 
                                    </div>
                            <?php endif;
                            endforeach; ?>
                            <!-- <img class="me-2" src="<?= base_url(); ?>/assets/images/dummy_img.png" alt=""> -->
                            <!-- <img class="me-2" src="<?= base_url(); ?>/assets/images/dummy_img.png" alt=""> -->
                        </div>
                        <div class="text-center my-4">
                            <h4><?= $a->judul; ?></h4>
                            <p><?= date('l', strtotime($a->tanggal)); ?>, <?= date('d F Y', strtotime($a->tanggal)); ?></p>
                        </div>
                        <div class="bg-card">
                            <p class="text-center fw-bold" style="font-size: 18px;">Dekripsi Kegiatan :</p>
                            <p><?= $a->deskripsi; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</div>
</div>
