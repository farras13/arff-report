<!-- Content -->
<div class="col py-3 mx-3">
    <h3 class="d-none d-md-block">Shipping</h3>
	<?php if($this->session->userdata('item') != 3): ?>
    <div class="p-4 card-shipping">
		<div class="row">
			<div class="col-6">
				<div class="form-shipping">
					<h4>Barang Masuk</h4>
					<button type="button" class="btn btn-white" data-bs-toggle="modal" data-bs-target="#exampleModalTerima">
						+
					</button>
	
				</div>
			</div>
			<div class="col-6">
				<div class="form-shipping">
					<h4>Barang Keluar</h4>
					<button type="button" class="btn btn-white" data-bs-toggle="modal" data-bs-target="#exampleModalKirim">
						+
					</button>
		
				</div>
			</div>
		</div>
    </div>
	<?php endif; ?>
    <div class="mt-4 table-responsive" style="margin-left: 30px;margin-right: 30px;">
        <table id="table_id" class="display table">
            <thead>
                <tr>
                    <th  style="text-align: center;">No AWB</th>
                    <th style="text-align: center;">Barang</th>
                    <th style="text-align: center;">Pengirim</th>
                    <th style="text-align: center;">Penerima</th>
                    <!-- <th style="text-align: center;">Tujuan</th> -->
                    <th style="text-align: center;">Hari/Tanggal Dikirim</th>
                    <th style="text-align: center;">Hari/Tanggal Diterima</th>
                    <th style="text-align: center;">Type</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">Berat</th>
                    <th style="text-align: center;">Jumlah</th>
                    <th style="text-align: center;">Kubikasi</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody style="vertical-align: middle;">
                <?php foreach ($data as $v) : ?>
                    <tr>
                        <td  style="text-align: center;"><?= $v->awb; ?></td>
                        <td  style="text-align: center;"><?= $v->nama_brg; ?></td>
						<td style="text-align: center;"><?= $v->pengirim; ?></td>
						<td style="text-align: center;"><?= $v->tujuan; ?></td>
                        <!-- <td style="text-align: center;"><b> <?= $v->pengirim == 'own' ? 'Rajawali Perkasa Logistic' : $v->pengirim; ?> - <?= $v->tujuan == 'own' ? 'Rajawali Perkasa Logistic' : $v->tujuan; ?> </b></td> -->
                        <td style="text-align: center;"><?= date('l', strtotime($v->tanggal_pengiriman)); ?>, <?= date('d F Y', strtotime($v->tanggal_pengiriman)); ?></td>

                        <?php if ($v->tanggal_diterima == "1970-01-01" || $v->tanggal_diterima == null || $v->tanggal_diterima == "0000-00-00") { ?>
                            <td style="text-align: center;">N/A</td>
                        <?php } else { ?>
                            <td style="text-align: center;"><?= date('l', strtotime($v->tanggal_diterima)); ?>, <?= date('d F Y', strtotime($v->tanggal_diterima)); ?></td>
                        <?php } ?>
						<td><?= $v->type == 0 ? 'Barang Keluar' : 'Barang Masuk'; ?></td>
                        <td>
                            <center>
                            <?php if ($v->status == "Dikirim") : ?>
                                <div class="status-dikirim">
                                    <p class="m-0"><b>PROCESS</b></p>
                                </div>
                            <?php elseif ($v->status == "Diterima") : ?>
                                <div class="status-diterima">
                                    <p class="m-0"><b>DITERIMA</b></p>
                                </div>
                            <?php endif; ?>
                            </center>
                        </td>
                        <td style="text-align: center;"><?= number_format($v->total_berat,0,',','.'); ?> Kg</td>
                        <td style="text-align: center;"><?= number_format($v->total_barang,0,',','.'); ?> Karton</td>
                        <td style="text-align: center;"><?= number_format($v->total_kubikasi,2,',','.'); ?></td>
                        <td>
                            <center>
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img width="35px" src="./assets/images/dot_actions.png" alt="" srcset="">
                                </button>
                                <ul class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton1">
                                    <!-- <a href="<?= base_url('Shipping/detail/') . $v->id_shiping ?>" target="_blank"><li class="dropdwon-item">Detail</li></a> -->
                                    <!-- <hr> -->
                                    <!-- <li class="dropdwon-item"><a href="<?= base_url('Shipping/print/') . $v->id_shiping ?>" target="_blank">Print</a></li> -->
                                    
                                    <!-- <hr> -->
                                    <!-- <li class="dropdwon-item"><a href="<?= base_url('Shipping/print/') . $v->id_shiping ?>" target="_blank">Download PDF</a></li> -->
                                    <?php if ($v->status != "Diterima") : ?>
                                    <hr>
                                    <li class="dropdwon-item" data-bs-toggle="modal" data-bs-target="#exampleModal<?= $v->id_shiping; ?>" style="font-weight: 700;">Konfirmasi <br>
                                        DITERIMA
                                    </li>
                                    <?php endif; ?>
                                    <?php if ($this->session->userdata('role') != 3) : ?>
                                    <hr>
                                    <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Shipping/del_data/') . $v->id_shiping ?>">Delete</a>
                                    </li>
                                    <?php endif; ?>
                                    
                                </ul>
                            </div>
                             </center>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
<!-- Modal Tambah Pengiriman Barang -->
<div class="modal fade" id="exampleModalKirim" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Pengiriman Barang</h4>
                <form action="<?= base_url('Shipping/ins_data_kirim'); ?>" method="post">
                    <div class="row g-2">
						<div class="col-4">
                            <p class="m-0">No AWB</p>
                        </div>
                        <div class="col-8">
                            <input type="text" name="awb" id="awb" placeholder="Input No AWB" required/>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Nama Barang</p>
                        </div>
                        <div class="col-8">
                            <input class="barang" type="text" name="barang" id="barang" list="browsers" autocomplete="off" placeholder="Input Nama Barang" />
                            <datalist id="browsers">
                                <?php foreach ($barang as $g) : ?>
                                    <option value="<?= $g->nama_brg ?>"><?= $g->nama_brg ?> - <?= $g->kondisi ?></option>
                                <?php endforeach; ?>
                            </datalist>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Jumlah Barang</p>
                        </div>
                        <div class="col-8">
                            <input type="text" class="jumlah" name="jumlah" id="jumlah" placeholder="Input Jumlah Barang Baik" />
                        </div>
						<div class="col-4">
                            <!-- <p class="m-0">Jml Barang Rusak</p> -->
                        </div>
                        <div class="col-8">
                            <input type="text" class="jumlah_rsk" name="jumlah_rsk" id="jumlah_rsk" placeholder="Input Jumlah Barang Rusak" />
                        </div>
						<div class="col-4">
                            <!-- <p class="m-0">Jml Barang Expired</p> -->
                        </div>
                        <div class="col-8">
                            <input type="text" class="jumlah_exp" name="jumlah_exp" id="jumlah_exp" placeholder="Input Jumlah Barang Expired" />
                        </div>
						<div class="col-4">
                            <p class="m-0">Berat Barang</p>
                        </div>
                        <div class="col-8">
                            <input type="text" class="berat" name="berat" id="berat" placeholder="Input Berat Barang" />
                        </div>
						<div class="col-4">
                            <p class="m-0">Kubikasi Barang</p>
                        </div>
                        <div class="col-8">
                            <input type="text" class="kubikasi" name="kubikasi" id="kubikasi" placeholder="Input Kubikasi Barang" />
                        </div>
						<div class="col-4">
                            <p class="m-0">Service Pengiriman</p>
                        </div>
                        <div class="col-8">
                            <select name="service" id="service">
								<option value="0">Reguler</option>
								<option value="1">Express</option>
								<option value="2">ONS</option>
							</select>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Tanggal Pengiriman</p>
                        </div>
                        <div class="col-8">
                            <input type="date" name="tgl_kirim" id="tgl_kirim" value="<?= date('Y-m-d'); ?>" placeholder="Input Jumlah Barang" />
                        </div>						
                        <div class="col-4">
                            <p class="m-0">Pengirim Barang</p>
                        </div>
                        <div class="col-8">
							<input type="text" name="pengirim" id="pengirim" placeholder="Asal Pengiriman" />
                        </div>
						<div class="col-4">
							<p class="m-0">Penerima Barang</p>
                        </div>
                        <div class="col-8">
							<input type="text" name="tujuan" id="tujuan" placeholder="Tujuan Pengiriman" />
                        </div>
						
                        <div class="col-4"></div>
                        <div class="col-8">
                            <button type="submit">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Tambah Pengiriman Barang -->
<div class="modal fade" id="exampleModalTerima" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Pengiriman Barang</h4>
                <form action="<?= base_url('Shipping/ins_data_terima'); ?>" method="post">
                    <div class="row g-2">
						<div class="col-4">
                            <p class="m-0">No AWB</p>
                        </div>
                        <div class="col-8">
                            <input type="text" name="awb" id="awb" placeholder="Input No AWB" required/>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Nama Barang</p>
                        </div>
						<div class="col-8">
                        <input class="barang" type="text" name="barang" id="barang" list="browsers" autocomplete="off" placeholder="Input Nama Barang" />
                            <datalist id="browsers">
                                <?php foreach ($barang as $g) : ?>
                                    <option value="<?= $g->nama_brg ?>"><?= $g->nama_brg ?> - <?= $g->kondisi ?></option>
                                <?php endforeach; ?>
                            </datalist>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Jumlah Barang</p>
                        </div>
                        <div class="col-8">
                            <input type="text" class="jumlah" name="jumlah" id="jumlah" placeholder="Input Jumlah Barang Baik" />
                        </div>
						<div class="col-4">
                            <!-- <p class="m-0">Jml Barang Rusak</p> -->
                        </div>
                        <div class="col-8">
                            <input type="text" class="jumlah_rsk" name="jumlah_rsk" id="jumlah_rsk" placeholder="Input Jumlah Barang Rusak" />
                        </div>
						<div class="col-4">
                            <!-- <p class="m-0">Jml Barang Expired</p> -->
                        </div>
                        <div class="col-8">
                            <input type="text" class="jumlah_exp" name="jumlah_exp" id="jumlah_exp" placeholder="Input Jumlah Barang Expired" />
                        </div>
						<div class="col-4">
                            <p class="m-0">Berat Barang</p>
                        </div>
                        <div class="col-8">
                            <input type="text" class="berat" name="berat" id="berat" placeholder="Input Berat Barang" />
                        </div>
						<div class="col-4">
                            <p class="m-0">Kubikasi Barang</p>
                        </div>
                        <div class="col-8">
                            <input type="text" class="kubikasi" name="kubikasi" id="kubikasi" placeholder="Input Kubikasi Barang" />
                        </div>
						<div class="col-4">
                            <p class="m-0">Service Pengiriman</p>
                        </div>
                        <div class="col-8">
                            <select name="service" id="service">
								<option value="0">Reguler</option>
								<option value="1">Express</option>
								<option value="2">ONS</option>
							</select>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Tanggal Pengiriman</p>
                        </div>
                        <div class="col-8">
                            <input type="date" name="tgl_kirim" id="tgl_kirim" value="<?= date('Y-m-d'); ?>" placeholder="Input Jumlah Barang" />
                        </div>						
                        <div class="col-4">
                            <p class="m-0">Pengirim Barang</p>
                        </div>
                        <div class="col-8">
							<input type="text" name="pengirim" id="pengirim" placeholder="Asal Pengiriman" />
                        </div>
						<div class="col-4">
							<p class="m-0">Penerima Barang</p>
                        </div>
                        <div class="col-8">
							<input type="text" name="tujuan" id="tujuan" placeholder="Tujuan Pengiriman" />
                        </div>
						
                        <div class="col-4"></div>
                        <div class="col-8">
                            <button type="submit">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php foreach ($data as $d) : ?>
    <!-- Modal Konfirmasi Penerimaan Barang -->
    <div class="modal fade" id="exampleModal<?= $d->id_shiping; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Konfirmasi Penerimaan Barang</h4>
                    <form action="<?= base_url('Shipping/upd_status/') . $d->id_shiping; ?>" method="post">
                        <div class="row g-2">
                            <div class="col-4">
                                <p class="m-0">Jumlah Barang</p>
                            </div>
                            <div class="col-8">
                                <input type="text" name="jumlah" id="jumlah" value="<?= $d->jumlah; ?>" readonly />
                            </div>
                            <div class="col-4">
                                <p class="m-0">Tanggal Penerimaan</p>
                            </div>
                            <div class="col-8">
                                <input type="date" name="tgl_diterima" id="tgl_diterima" value="<?= date('Y-m-d'); ?>" readonly />
                            </div>
                            <div class="col-4"></div>
                            <div class="col-8">
                                <button type="submit">Konfirmasi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>


<script>
	$('.modal').on('hidden.bs.modal', function (e) {
		$(this)
			.find("input,textarea,select")
				.val('')
				.end()
			.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
	})
	$(document).on('change', '.barang', function() {
		var a = $(this).val();
		console.log(a);
		// var b = $('#nopro').val();
		// console.log(a);
		$.post('<?php echo site_url('Shipping/getStok') ?>', {
			nama: a,
		}, function(data) {
			if (data != "" || data != null) {
				var obj = JSON.parse(data);
				$('.jumlah').val(obj.baik);
				$('.jumlah_rsk').val(obj.rusak);
				$('.jumlah_exp').val(obj.expired);
				$('.berat').val(obj.berat);
				$('.kubikasi').val(obj.kubikasi);
				
			}
		});
		console.log("halo");
	});
</script>
