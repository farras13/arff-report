<!-- Content -->
<div class="col py-3 mx-3">
    <h3 class="d-none d-md-block">Gudang</h3>
	<?php if($this->session->userdata('item') != 3): ?>
		<div class="p-4 card-storage">
			<!-- <div class="form-storage"> -->
			<form class="form-storage" action="<?= base_url('Gudang/ins_data'); ?>" method="POST" enctype="multipart/form-data" onsubmit="kirimform()" novalidate>
				<div class="bg-white rounded-form p-2">
					<div class="dropzone" id="gudang">

					</div>
					<input type="hidden" id="gambar" name="gambar">
				</div>
				<div class="card border-0 rounded-form p-2">
					<div class="card-body">
						<h4 class="d-none d-md-block">Tambah Barang</h4>
						<div class="row g-2">
							<div class="col-4">
								<p class="m-0">Nama Barang</p>
							</div>
							<div class="col-8">
								<input type="text" name="nama" id="nama" list="browsers" autocomplete="off" placeholder="Input Nama Barang" />
								<datalist id="browsers">
									<?php foreach ($data as $g) : ?>
										<option value="<?= $g->nama_brg ?>"><?= $g->nama_brg ?></option>
									<?php endforeach; ?>
								</datalist>
							</div>
							<div class="col-4">
								<p class="m-0">Jumlah Barang</p>
							</div>
							<div class="col-8">
								<input type="number" name="jumlah" id="jumlah" autocomplete="off" placeholder="Input Jumlah Barang" />
							</div>
							<div class="col-4">
								<p class="m-0">Berat Barang</p>
							</div>
							<div class="col-8">
								<input type="number" name="berat" id="berat" autocomplete="off" placeholder="Input Berat Barang" />
							</div>
							<div class="col-4">
								<p class="m-0">Kubikasi Barang</p>
							</div>
							<div class="col-8">
								<input type="number" name="kubikasi" id="kubikasi" step="any" autocomplete="off" placeholder="Input kubikasi Barang" />
							</div>
							<div class="col-4">
								<p class="m-0">Status</p>
							</div>
							<div class="col-8">
								<select name="kondisi" id="kondisi" autocomplete="off" >
									<option value="">Pilih Kondisi Barang</option>
									<option value="Baik">Baik</option>
									<option value="Rusak">Rusak</option>
									<option value="Expired">Expired</option>
								</select>
							</div>
							<div class="col-4"></div>
							<div class="col-8">
								<button type="submit">Submit</button>
							</div>
						</div>
					</div>
					<div class="d-md-none row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" id="nama_barang" name="namam" >
								<label for="nama_barang">Nama Barang</label>
							</div>
							<div class="form-group">
								<input type="number" class="form-control" id="nama_barang" name="jumlahm" >
								<label for="nama_barang">Jumlah Barang</label>
							</div>
							<div class="form-group">
								<select name="kondisim" id="" >
									<option value="">Pilih Kondisi Barang</option>
									<option value="Baik">Baik</option>
									<option value="Rusak">Rusak</option>
									<option value="Expired">Expired</option>
								</select>
							</div>
						</div>
					</div>
					<button class="d-sm-block d-md-none mt-3" type="submit">Submit</button>
				</div>
			</form>
			<!-- </div> -->
		</div>
	<?php endif; ?>
    <div class="mt-4 table-responsive">
        <table id="table_id" class="display table">
            <thead>
                <tr>
                    <th>Nama Barang</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">Jumlah</th>
                    <th style="text-align: center;">Berat</th>
                    <th style="text-align: center;">Kubikasi</th>
                    <th style="text-align: center;">Penginput</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody style="vertical-align: middle;">
                <?php foreach ($data as $d) : ?>
                    <tr>
                        <td><img src="<?= $d->image ?>" alt="" width="50px" height="50px" style="margin-right: 35px;"><?= $d->nama_brg != null ? $d->nama_brg : "https://warehouse.rajawaliperkasalogistic.com/assets/images/LOGO.png"; ?></td>
                        <td style="text-align: center;">
                            <center>
                            <?php if ($d->kondisi == "Baik") : ?>
                                <div class="pill-baik me-2">
                                    <p class="m-0 fw-bold"><?= $d->kondisi ?></p>
                                </div>
                            <?php elseif ($d->kondisi == "Rusak") : ?>
                                <div class="pill-rusak me-2">
                                    <p class="m-0 fw-bold">Rusak</p>
                                </div>
                            <?php else : ?>
                                <div class="pill-kosong me-2">
                                    <p class="m-0 fw-bold"><?= $d->kondisi ?></p>
                                </div>
                            <?php endif; ?>
                            </center>

                        </td>
                        <td style="text-align: center;"><?= number_format($d->stok,0,',','.'); ?></td>
                        <td style="text-align: center;"><?= number_format($d->berat,2,',','.'); ?></td>
                        <td style="text-align: center;"><?= number_format($d->kubikasi,2,',','.'); ?></td>
                        <td style="text-align: center;"><?= $d->username; ?></td>
                        <td>
                            <center>
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img width="35px" src="./assets/images/dot_actions.png" alt="" srcset="">
                                </button>
                                <ul class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton1">
                                    <a href="<?= base_url('Gudang/detail/') . $d->id_gudang ?>" target="_blank"><li class="dropdwon-item">Detail</li></a>
                                    <hr>
									<li class="dropdwon-item" data-bs-toggle="modal" data-bs-target="#updateModal<?= $d->id_gudang ?>" style="font-weight: 700; color: #0d6efd;">Update Status</li>
                                    <!-- <li class="dropdwon-item"><a href="<?= base_url('Gudang/print/') . $d->id_gudang; ?>" target="_blank">Print</a></li> -->
                                    <!-- <hr> -->
                                    <!-- <li class="dropdwon-item"><a href="<?= base_url('Gudang/print/') . $d->id_gudang; ?>" target="_blank">Download PDF</a></li> -->
                                    <?php if ($this->session->userdata('role') != 3) {  ?>
                                        <hr>
                                        <li class="dropdwon-item"><a href="<?= base_url('Gudang/del_data/') . $d->id_gudang ?>">Delete</a>
                                        </li>
                                        <hr>
                                        <!-- <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Gudang/upd_status/') . $d->id_gudang . '/Baik'; ?>">BAIK</a>
                                        </li>
                                        <hr>
                                        <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Gudang/upd_status/') . $d->id_gudang . '/Rusak'; ?>">RUSAK</a>
                                        </li>
                                        <hr>
                                        <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Gudang/upd_status/') . $d->id_gudang . '/Expired'; ?>">Expired</a>
                                        </li> -->
                                    <?php } ?>
                                </ul>
                            </div>
                            </center>
                        </td>

                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
</div>
</div>

<!-- Modal Tambah Pengiriman Barang -->
<?php foreach($data as $dt): ?>
<div class="modal fade" id="updateModal<?= $dt->id_gudang ?>" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Update Status Barang</h4>
                <form action="<?= base_url('Gudang/upd_data'); ?>" method="post">					
                    <div class="row g-2">
						<!-- <div class="col-12 my-2"><h6><b>Status Baik</b></h6></div>						 -->
                        <div class="col-4">
                            <p class="m-0">Jumlah Barang</p>
                        </div>
                        <div class="col-8">
							<input type="hidden" name="id" value="<?= $dt->id ?>">
                            <input type="text" name="jumlah" id="jumlah" value="<?= $dt->stok ?>"/>
                        </div>				
                        <div class="col-4"></div>
                        <div class="col-8">
                            <button type="submit">Kirim</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>

<script>
	$(document).on('change', '#nama', function() {
		var a = $(this).val();
		// var b = $('#nopro').val();
		// console.log(a);
		$.post('<?php echo site_url('Gudang/getstok') ?>', {
			name: a,
		}, function(data) {
			if (data != "" || data != null) {
				var obj = JSON.parse(data);
				$('#berat').val(obj.berat);
				$('#kubikasi').val(obj.kubikasi);
				
			}
		});
		console.log("halo");
	});
</script>
