<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>LAPORAN KEGIATAN OPERASIONAL</title>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    <style>
        @page {
            size: A4;
            margin: 15mm 15mm 15mm 15mm;
        }

        .sheet {
            overflow: visible;
            height: auto !important;
        }

        * {
            font-family: sans-serif;
            margin: 20px, 10px, 20px, 10px;
        }

        img {
            width: 180px;
        }

        .img {
            width: 60px;
            text-align: center;
        }

        h1 {
            font-weight: bold;
            font-size: 20pt;
            text-align: center;
        }

        .dokumentasi {
            margin-left: 20px;
            margin-top: 15px;
            margin-bottom: 10px;
            padding: 8px;
        }

        .ttd {
            position: relative;
            margin-top: 160px;
        }

        .ttd-kolom {
            position: absolute;
            right: 0px;
            width: 300px;
            height: 200px;
            text-align: center;
            margin-right: 15px;
        }

        .ttd-kolom-bawah {
            position: absolute;
            right: 0px;
            top: 185px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        .table th {
            padding: 8px 8px;
            border: 1px solid #000000;
            text-align: center;
        }

        .table td {
            padding: 3px 3px;
            border: 1px solid #000000;
            text-align: center;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body class="A4" onload="window.print()">

    <section class="sheet padding-10mm">
        <table>
            <tr>
                <td colspan="2"><img src="<?= base_url('assets/images/logo_with_name.png'); ?>" alt=""></td>
            </tr>
            <?php foreach ($data as $data) : ?>
                <tr>

                    <td>
                        <center>
                            <h3><b>RINCIAN LAPORAN KEGIATAN<br>
                                    KANTOR UNIT PENYELENGGARA BANDAR UDARA KELAS III INANWATAN <br>
                                    UNIT PERTOLONGAN KECELAKAAN PENERBANGAN (PKP-PK)</b></h3>
                        </center>
                        <center>
                            <h3><b><u>LAPORAN KEGIATAN OPERASIONAL PKP-PK</u><br> NO:
                                    <?= $day; ?>/<?= $bln; ?>/<?= $thn; ?>/INX/PKP-PK/KEG/<?= $no; ?></b></h3>
                        </center>
                    </td>
                </tr>
        </table>
        <table style="margin-left: 20px; margin-top: 20px;">
            <tbody>
                <tr>
                    <td width="180"><b>JUDUL KEGIATAN</b></td>
                    <td class="text-center" width="20">:</td>
                    <td><?= $data->judul; ?></td>
                </tr>
                <tr>
                    <td><b>HARI/TANGGAL</b></td>
                    <td class="text-center">:</td>
                    <?php if ($data->tanggal == "1970-01-01" || $data->tanggal == null || $data->tanggal == "0000-00-00") { ?>
                        <td>N/A</td>
                    <?php } else { ?>
                        <td><?= date('l', strtotime($data->tanggal)); ?>, <?= date('d F Y', strtotime($data->tanggal)); ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td><b>STATUS</b></td>
                    <td class="text-center">:</td>
                    <td><?php if ($data->status == 0) {
                            echo '<b>Belum SELESAI</b>';
                        } else {
                            echo '<b>SELESAI</b>';
                        } ?>
                    </td>
                </tr>
                <tr>
                    <td><b>KONFIRMASI KANIT</b></td>
                    <td class="text-center">:</td>
                    <td><?php if ($data->is_seen == 0) {
                            echo '<b>BELUM DIPERIKSA</b>';
                        } else {
                            echo '<b>SUDAH DIPERIKSA</b>';
                        } ?>
                    </td>
                </tr>
                <tr>
                    <td><b>DESKRIPSI KEGIATAN</b></td>
                    <td class="text-center">:</td>
                </tr>
            </tbody>
        </table>
    <?php endforeach; ?>
    <div class="dokumentasi"> <textarea name="" id="" cols="90" rows="10" disabled><?= $data->deskripsi; ?> </textarea>

    </div>
    <?php if ($data->is_seen != 0) { ?>
        <div class="ttd">
            <table class="ttd-kolom">
                <tr>
                    <td> Mengetahui,<br>Sorong, <?= date('d F Y', strtotime($data->tanggal)); ?><br><b>KEPALA UNIT
                            PKP-PK</b></td>
                </tr>
                <tr>
                    <td>
                        <img src="<?= base_url('assets/images/stempel.jpg'); ?>" alt="">
                    </td>
                </tr>
                <tr>
                    <td>NASRUN<br>Pengatur II/c<br><b>NIP.19840921 201012 1 001</b></td>
                </tr>
            </table>
        </div>
    <?php } else { ?>
        <div class="ttd">
            <table class="ttd-kolom">
                <tr>
                    <td> Mengetahui,<br>Sorong, <?= date('d F Y'); ?><br><b>KEPALA UNIT PKP-PK</b></td>
                </tr>
                <tr>
                    <td style="margin-top: 120px; margin-bottom: 120px;">
                        <img src="<?= base_url('assets/images/stempel.jpg'); ?>" alt="" hidden>
                    </td>
                </tr>
                <tr>
                    <td><u><b><u><b>NASRUN</b></u></b></u><br>Pengatur II/c<br><b>NIP.19840921 201012 1 001</b></td>
                </tr>
            </table>
        </div>
    <?php } ?>

    </section>
    <section class="sheet padding-10mm">
        <p>Dokumentasi</p>
        <div class="dokumentasi">
            <?php foreach ($gambar as $g) : ?>
                <img src="<?= $g->image; ?>" alt="dokumentasi barang">
            <?php endforeach; ?>
        </div>
    </section>

</body>

</html>