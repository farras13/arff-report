<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>LAPORAN PENGIRIMAN BARANG</title>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    <style>
        @page {
            size: A4;
            margin: 15mm 15mm 15mm 15mm;
        }

        .sheet {
            overflow: visible;
            height: auto !important;
        }

        * {
            font-family: sans-serif;
            margin: 20px, 10px, 20px, 10px;
        }

        img {
            width: 180px;
        }

        .img {
            width: 60px;
            text-align: center;
        }

        h1 {
            font-weight: bold;
            font-size: 20pt;
            text-align: center;
        }

        .dokumentasi {
            margin-left: 20px;
            margin-top: 15px;
            margin-bottom: 10px;
            padding: 8px;
        }

        .ttd {
            position: relative;
            margin-top: 160px;
        }

        .ttd-kolom {
            position: absolute;
            right: 28px;
            top: 145px;
        }

        .ttd-kolom-bawah {
            position: absolute;
            right: 10px;
            top: 285px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        .table th {
            padding: 8px 8px;
            border: 1px solid #000000;
            text-align: center;
        }

        .table td {
            padding: 3px 3px;
            border: 1px solid #000000;
            text-align: center;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body class="A4" onload="window.print()">

    <section class="sheet padding-10mm">
        <table>
            <tr>
                <td colspan="2"><img src="<?= base_url('assets/images/logo_with_name.png'); ?>" alt=""></td>
            </tr>
            <tr>

                <td>
                    <center>
                        <h3><b>RINCIAN LAPORAN PENGIRIMAN BARANG<br>
                                KANTOR UNIT PENYELENGGARA BANDAR UDARA KELAS III INANWATAN <br>
                                UNIT PERTOLONGAN KECELAKAAN PENERBANGAN (PKP-PK)</b></h3>
                    </center>
                    <center>
                        <h3><b><u>LAPORAN PENGIRIMAN BARANG OPERASIONAL PKP-PK</u><br> NO:
                                <?= $day; ?>/<?= $bln; ?>/<?= $thn; ?>/INX/PKP-PK/KRM/<?= $no; ?></b></h3>
                    </center>
                </td>
            </tr>
        </table>

        <table style="margin-left: 20px; margin-top: 20px;">
            <tbody>
                <tr>
                    <td width="180"><b>NAMA BARANG</b></td>
                    <td class="text-center" width="20">:</td>
                    <td><?= $data->nama_brg; ?></td>
                </tr>
                <tr>
                    <td><b>TANGGAL DIKIRIM</b></td>
                    <td class="text-center">:</td>
                    <?php if ($data->tanggal_diterima == "1970-01-01" || $data->tanggal_diterima == null || $data->tanggal_diterima == "0000-00-00") { ?>
                        <td>N/A</td>
                    <?php } else { ?>
                        <td><?= date('l', strtotime($data->tanggal_diterima)); ?>, <?= date('d F Y', strtotime($data->tanggal_diterima)); ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td><b>TANGGAL DITERIMA</b></td>
                    <td class="text-center">:</td>
                    <td><?= date('l', strtotime($data->tanggal_diterima)); ?>, <?= date('d F Y', strtotime($data->tanggal_diterima)); ?></td>
                </tr>
                <tr>
                    <td><b>PENGIRIM</b></td>
                    <td class="text-center">:</td>
                    <td><?= $data->name; ?></td>
                </tr>
                <tr>
                    <td><b>TUJUAN</b></td>
                    <td class="text-center">:</td>
                    <?php if ($data->tujuan == "SOQ") { ?>
                        <td><b> INX - <?= $data->tujuan; ?> </b></td>
                    <?php } else { ?>
                        <td><b> SOQ - <?= $data->tujuan; ?> </b></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td><b>JUMLAH</b></td>
                    <td class="text-center">:</td>
                    <td><?= $data->jumlah; ?></td>
                </tr>
                <tr>
                    <td><b>STATUS</b></td>
                    <td class="text-center">:</td>
                    <td><?= $data->status; ?></td>
                </tr>
                <tr>
                    <td><b>DOKUMENTASI</b></td>
                    <td class="text-center">:</td>

                </tr>
            </tbody>
        </table>
        <div class="dokumentasi">
            <?php foreach ($gambar as $g) : ?>
                <img src="<?= $g->image; ?>" alt="dokumentasi barang">
            <?php endforeach; ?>
        </div>
        <div class="ttd">
            <div class="ttd-kolom">
                <center>
                    <span style="margin-top: 15px;">Mengetahui,<br>Sorong, <?= date('d F Y'); ?><br><b>KEPALA UNIT
                            PKP-PK</b></span>
                </center>
            </div>

            <div class="ttd-kolom-bawah" style="margin-top: 30px;">
                <center>
                    <span><u><b>NASRUN</b></u><br>Pengatur II/c<br><b>NIP. 19840921 201012 1 001</b></span>
                </center>
            </div>
        </div>
    </section>
</body>

</html>