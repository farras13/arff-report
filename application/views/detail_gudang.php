<div class="col py-3 mx-3">
    <h3>Detail Gudang</h3>
     <div class="p-4 card-aktifitas" style="margin:10px;">
        <div class="bg-white rounded-form p-2">
            <?php foreach ($data as $d) : ?>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>NAMA BARANG</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->nama_brg; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>WAKTU INPUT</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= date('d F Y', strtotime($d->created)); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>PENGINPUT</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->username; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>KONDISI</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                       <?= $d->kondisi ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>JUMLAH</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->stok; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>DOKUMENTASI</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-12">
                        <?php foreach ($dokumentasi as $g) : ?>
                            <img src="<?= $g->image; ?>" alt="dokumentasi barang" width="510">
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>



    </div>

</div>
</div>
</div>
