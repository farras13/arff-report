<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/index.css">
    <!-- <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" /> -->
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <!-- <title>Hello, world!</title> -->
</head>

<body>
    <div class="d-sm-block d-md-none">
        <div class="overlay">a</div>
        <div class="sidebar-overlay">
            <div class="d-flex justify-content-end p-3">
                <img src="<?= base_url(); ?>/assets/images/icon_notif_white.png" class="me-2" width="20px" alt="" srcset="">
                <img src="<?= base_url(); ?>/assets/images/icon_white_sidebar.png" width="20px" alt="" srcset="">
            </div>
            <div class="d-flex flex-column p-3" style="gap: 15px; margin-bottom: 10rem;">
                <a href="" class="btn sidebar-btn">Dashboard</a>
                <a href="" class="btn sidebar-btn">Aktivitas</a>
                <a href="" class="btn sidebar-btn">Gudang</a>
                <a href="" class="btn sidebar-btn">Pengiriman</a>
            </div>
            <div class="d-flex justify-content-around">
                <a href="" class="btn sidebar-btn"><img class="me-2" src="<?= base_url(); ?>/assets/images/icon_people.png" width="18px" alt="" srcset=""><b>Akun</b></a>
                <a href="" class="btn sidebar-btn">Keluar</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Navbar for Mobile -->
        <div class="d-sm-block d-md-none">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a href="<?= base_url('Dashboard'); ?>" class="d-flex align-items-center navbar-brand text-decoration-none">
                        <img src="<?= base_url(); ?>/assets/images/LOGO WEB ARFF 1.png" width="25px" />
                        <span class="sidebar-title ms-3"><b>ARFF Report</b> | Dashboard</span>
                    </a>
                    <img src="<?= base_url(); ?>/assets/images/icon_notif.png" class="ms-auto me-2" width="20px" alt="" srcset="">
                    <a href="#" class="triggerSidebar">
                        <img src="<?= base_url(); ?>/assets/images/icon_sidebar.png" width="20px" alt="" srcset="">
                    </a>
                </div>
            </nav>
        </div>
        <div class="row flex-nowrap overflow-hidden">
            <!-- Sidebar for Dekstop -->
            <div class="d-none d-md-block col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-white sidebar">
                <div class="d-flex flex-column align-items-center align-items-sm-start pt-3 shadow-sm position-fixed" style="height: 100%;">
                    <a href="/" class="d-flex align-items-center align-middle pb-3 px-3 mb-md-0  text-decoration-none">
                        <div class="text-center">
                            <img class="mx-auto d-block" src="<?= base_url(); ?>/assets/images/LOGO WEB ARFF 1.png" width="25px" />
                        </div>
                        <span class="fs-5 d-none d-lg-inline sidebar-title ms-3">ARFF Report</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start w-100" id="menu">
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Dashboard" || $this->uri->segment(1) == "") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Dashboard'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_dashboard.png" width="20px" alt=""></i>
                                <span class="ms-2 d-none d-lg-inline">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Aktivitas" || $this->uri->segment(1) == "") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Aktivitas'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_activity.png" width="20px" alt=""></i>
                                <span class="ms-2 d-none d-lg-inline">Activity</span>
                            </a>
                        </li>
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Gudang" || $this->uri->segment(1) == "") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Gudang'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_storage.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-lg-inline">Storage</span>
                            </a>
                        </li>
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Shipping" || $this->uri->segment(1) == "") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Shipping'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_shipping.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-lg-inline">Shipping</span>
                            </a>
                        </li>
                        <?php if ($this->session->userdata('role') == 1) : ?>
                            <hr class="w-100">
                            <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Setting" || $this->uri->segment(1) == "") {
                                                        echo "active";
                                                    } ?>">
                                <a href="<?= base_url('Setting'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                    <i><img src="<?= base_url(); ?>/assets/images/icon_setting.png" width="20px" alt=""></i></i>
                                    <span class="ms-2 d-none d-lg-inline">Settings</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <hr />
                    <div class="d-flex justify-content-between footer-sidebar align-items-center">
                        <!-- <img src="https://github.com/mdo.png" alt="hugenerd" width="40" height="40"
                            class="rounded-circle" /> -->
                        <div class="d-flex flex-column ms-2">
                            <span class="d-none d-lg-inline mx-1 footer-user"><?= $nama; ?></span>
                            <span class="d-none d-lg-inline mx-1 footer-nip">NIP. <?= $nip; ?></span>
                        </div>
                        <img src="<?= base_url(); ?>/assets/images/icon_logout.png" class="ms-2" width="15px" height="15px" alt="">
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col py-3 overflow-hidden">
                <h3 class="d-none d-md-block">Dashboard</h3>
                <div>
                    <p class="m-0 mt-4 text-md-start text-center">Aktivitas</p>
                    <div class="main-carousel bg-card">
                        <?php foreach ($aktivitas as $a) { ?>
                            <div class="carousel-cell" data-bs-toggle="modal" data-bs-target="#exampleModal<?= $a->id_aktivitas; ?>">
                                <div class="card card-aktivitas">
                                    <div class="card-body">
                                        <h5 class="card-title"> <?= $a->judul; ?></h5>
                                        <p class="card-text"><?= date('l', strtotime($a->tanggal)); ?>,<?= date('d F Y', strtotime($a->tanggal)); ?></p>
                                        <div class="text-center">
                                            <img width="100%" src="<?= base_url(); ?>/assets/images/dummy_img.png" alt="">
                                        </div>
                                        <p class="card-text mt-2">
                                            <?php if (strlen($a->deskripsi) > 50) {
                                                echo substr($a->deskripsi, 0, 50) . '...';
                                            } else {
                                                echo $a->deskripsi;
                                            }  ?>
                                        </p>
                                        <div class="d-flex justify-content-center" style="gap: 10px;">
                                            <div class="d-flex flex-row align-items-center">
                                                <?php if ($a->status == 0) { ?>
                                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">
                                                <?php } else { ?>
                                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                                <?php } ?>
                                                <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                                <p class="m-0 fw-bold status-aktivitas"><?php if ($a->status == 0) {
                                                                                            echo "BELUM SELESAI";
                                                                                        } else {
                                                                                            echo "SELESAI";
                                                                                        } ?></p>
                                            </div>
                                            <div class="d-flex flex-row align-items-center">
                                                <?php if ($a->is_seen == 0) { ?>
                                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">
                                                <?php } else { ?>
                                                    <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                                <?php } ?>
                                                <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">
                                                <p class="m-0 fw-bold status-aktivitas"> <?php if ($a->is_seen == 0) {
                                                                                                echo "BELUM DIPERIKSA";
                                                                                            } else {
                                                                                                echo "SUDAH DIPERIKSA";
                                                                                            } ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <a href="" class="btn btn-tambah d-sm-block d-md-none mt-4">+ Tambah Kegiatan</a>
                </div>
                <div>
                    <p class="m-0 mt-4 text-md-start text-center">Gudang</p>
                    <div class="bg-card">
                        <div class="row row-cols-3 align-items-start">
                            <?php foreach ($gudang as $g) : ?>
                                <?php if ($g->kondisi == "Baik") : ?>
                                    <div class="col mb-2">
                                        <div class="d-flex">
                                            <div class="bullet-count-baik me-2">
                                                <p class="m-0 mx-auto fw-bold"><?= $g->stok; ?></p>
                                            </div>
                                            <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                                        </div>
                                    </div>
                                <?php elseif ($g->kondisi == "Kosong") : ?>
                                    <div class="col mb-2">
                                        <div class="d-flex">
                                            <div class="bullet-count-kosong me-2">
                                                <p class="m-0 mx-auto fw-bold"><?= $g->stok; ?></p>
                                            </div>
                                            <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="col mb-2">
                                        <div class="d-flex">
                                            <div class="bullet-count-rusak me-2">
                                                <p class="m-0 mx-auto fw-bold"><?= $g->stok; ?></p>
                                            </div>
                                            <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <a href="" class="btn btn-tambah d-sm-block d-md-none mt-4">+ Tambah Barang</a>
                        <p class="last-updated d-none d-md-block">Terakhir diupdate : 11 Juni 2022</p>
                    </div>
                </div>
                <div class="d-none d-md-block">
                    <p class="m-0 mt-4 text-md-start text-center">Status Barang</p>
                    <div class="bg-card">
                        <div class="row row-cols-3 align-items-start">
                            <?php foreach ($gudang as $g) : ?>
                                <?php if ($g->kondisi == "Baik") : ?>
                                    <div class="col mb-2">
                                        <div class="d-flex">
                                            <div class="pill-baik me-2">
                                                <p class="m-0 mx-auto fw-bold">BAIK</p>
                                            </div>
                                            <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                                        </div>
                                    </div>
                                <?php elseif ($g->kondisi == "Kosong") : ?>
                                    <div class="col mb-2">
                                        <div class="d-flex">
                                            <div class="pill-kosong me-2">
                                                <p class="m-0 mx-auto fw-bold">KOSONG</p>
                                            </div>
                                            <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="col mb-2">
                                        <div class="d-flex">
                                            <div class="pill-rusak me-2">
                                                <p class="m-0 mx-auto fw-bold">RUSAK</p>
                                            </div>
                                            <p class="m-0 card-items"><?= $g->nama_brg; ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <p class="last-updated">Terakhir diupdate : 11 Juni 2022</p>
                    </div>
                </div>
                <div>
                    <p class="m-0 mt-4 text-md-start text-center">Pengiriman</p>
                    <div class="bg-card w-100 overflow-auto">
                        <div class="kontainer-pengiriman">
                            <?php $no = 1;
                            foreach ($ship as $s) : ?>
                                <div class="card-pengiriman">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <?php if ($s->tujuan == "SOQ") { ?>
                                            <div class="d-flex flex-row">
                                                <p class="m-0 fw-bold me-3">INX</p>
                                                <img src="<?= base_url(); ?>/assets/images/icon_arrow.png" alt="">
                                                <p class="m-0 fw-bold ms-3">SOQ</p>
                                            </div>
                                        <?php } else { ?>
                                            <div class="d-flex flex-row">
                                                <p class="m-0 fw-bold me-3">SOQ</p>
                                                <img src="<?= base_url(); ?>/assets/images/icon_arrow.png" alt="">
                                                <p class="m-0 fw-bold ms-3">INX</p>
                                            </div>
                                        <?php } ?>
                                        <div class="d-flex flex-column text-center my-2 my-md-4">
                                            <p class="m-0">Mesin Babat</p>
                                            <p class="m-0">2 Unit</p>
                                        </div>
                                        <?php if ($s->status == "Dikirim") : ?>
                                            <div class="status-dikirim">
                                                <p class="m-0">DIKIRIM</p>
                                            </div>
                                        <?php else : ?>
                                            <div class="status-diterima">
                                                <p class="m-0">DITERIMA</p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php $no++;
                                if ($no > 5) {
                                    break;
                                }
                            endforeach; ?>
                        </div>
                        <a href="" class="btn btn-tambah d-sm-block d-md-none mt-4">+ Tambah Pengiriman</a>
                    </div>
                </div>
                <!-- Modal -->
                <?php foreach ($aktivitas as $a) { ?>
                    <div class="modal" id="exampleModal<?= $a->id_aktivitas; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="d-flex justify-content-center" style="gap: 10px;">
                                        <div class="d-flex flex-row align-items-center">
                                            <?php if ($a->status == 0) { ?>
                                                <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">;
                                            <?php } else { ?>
                                                <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">;
                                            <?php } ?>
                                            <p class="m-0 fw-bold"><?php if ($a->status == 0) {
                                                                        echo "BELUM SELESAI";
                                                                    } else {
                                                                        echo "SELESAI";
                                                                    } ?></p>
                                        </div>
                                        <div class="d-flex flex-row align-items-center">
                                            <?php if ($a->is_seen == 0) { ?>
                                                <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_close.png" alt="">;
                                            <?php } else { ?>
                                                <img class="me-2" src="<?= base_url(); ?>/assets/images/icon_check.png" alt="">;
                                            <?php } ?>

                                            <p class="m-0 fw-bold"><?php if ($a->is_seen == 0) {
                                                                        echo "BELUM DIPERIKSA";
                                                                    } else {
                                                                        echo "SUDAH DIPERIKSA";
                                                                    } ?></p>
                                        </div>
                                    </div>

                                    <!-- Flickity HTML init -->
                                    <div class="gallery js-flickity mt-4" data-flickity-options='{  "percentPosition": false, "pageDots": false,  "adaptiveHeight": true,"wrapAround": true }'>
                                        <?php foreach ($galeri as $g) : if ($g->aktivitas_id == $a->id_aktivitas) : ?>
                                                <div class="carousel-cell">
                                                    <img class="me-2" src="<?= $g->image; ?>" alt="">
                                                </div>
                                        <?php endif;
                                        endforeach; ?>
                                        <!-- <img class="me-2" src="<?= base_url(); ?>/assets/images/dummy_img.png" alt=""> -->
                                        <!-- <img class="me-2" src="<?= base_url(); ?>/assets/images/dummy_img.png" alt=""> -->
                                    </div>
                                    <div class="text-center my-4">
                                        <h4><?= $a->judul; ?></h4>
                                        <p><?= date('l', strtotime($a->tanggal)); ?>, <?= date('d F Y', strtotime($a->tanggal)); ?></p>
                                    </div>
                                    <div class="bg-card">
                                        <p class="text-center fw-bold" style="font-size: 18px;">Dekripsi Kegiatan :</p>
                                        <p><?= $a->deskripsi; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <!-- <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script> -->
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <script>
        var flkty = new Flickity('.main-carousel', {
            // options
            cellAlign: "left",
            freeScroll: true,
            contain: true,
            // disable previous & next buttons and dots
            prevNextButtons: false,
            pageDots: false
        });

        $('#exampleModal').on('shown.bs.modal', function(event) {
            $('.gallery').flickity('resize');
        });

        $('.triggerSidebar').click(function() {
            $('.sidebar-overlay').css('right', '0px')
            $('.overlay').css('display', 'block')
        })

        var sembunyikan = function() {
            $('.overlay').css('display', 'none')
            $('.sidebar-overlay').css('right', '-300px')
        }

        $('.hideSidebar').click(sembunyikan)
        $('.overlay').click(sembunyikan)
    </script>
    <!-- <script>
        var swiper = new Swiper(".swiper", {});
    </script> -->
    <!-- <script>
        const swiper = new Swiper('.swiper', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script> -->

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->

</body>

</html>