<div class="modal fade" id="notif" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <table style="width: 100%;">
                    
                    <center>
                        <?php if ($this->session->userdata('role') != 3) { ?>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;"><b>Aktivitas</b></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                            <?php if($notif_admin['aktivitas'] == null){ ?>
                             <tr>
                                <td colspan="2"> <center> Belum ada kegiatan baru </center> </td>
                            </tr>
                            <?php }else{ ?>
                                <?php foreach ($notif_admin['aktivitas'] as $nk) { ?>
                                    <tr>
                                        <td> <?= $nk->judul; ?> </td>
                                        <td> <a href="<?= base_url('Aktivitas/upd_status/') . $nk->id_aktivitas; ?>" class="mx-2 btn btn-danger">Konfirmasi</a></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;"><b>Shipping</b></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr>
                            </td>
                        </tr>
                       <?php if ($this->session->userdata('role') == 3) { ?>
                            <?php if($notif_pegawai['shipping'] == null){ ?>
                                <tr>
                                    <td colspan="2"><center>Belum ada pengiriman</center></td>
                                </tr>
                            <?php }else{ foreach ($notif_pegawai['shipping'] as $sp) {  ?>
                                <tr>
                                    <td> <?= $sp->nama; ?> </td>
                                    <td> <a href="<?= base_url('Shipping/upd_status/') . $sp->id_shiping; ?>" class="mx-2 btn btn-danger" >Konfirmasi</a></td>
                                </tr>
                            <?php }} ?>
                        <?php } else { ?>
                            <?php if($notif_pegawai['shipping'] == null){ ?>
                                <tr>
                                    <td colspan="2"><center>Belum ada pengiriman</center></td>
                                </tr>
                            <?php }else{ foreach ($notif_admin['shipping'] as $sp) { ?>
                                <tr>
                                    <td> <?= $sp->nama; ?> </td>
                                    <td> <a href="<?= base_url('Shipping/upd_status/') . $sp->id_shiping; ?>" class="mx-2 btn btn-danger" >Konfirmasi</a></td>
                                </tr>
                            <?php }} ?>
                        <?php } ?>
                    </center>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<!-- <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script> -->
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
    $(document).ready(function() {
        $('#table_id').DataTable({
            "ordering": false
        });
    });
    <?php if (isset($_SESSION['toast'])) { ?>
        toastr.options.closeButton = true;
        var toastvalue = "<?php echo $_SESSION['toast'] ?>";
        var status = toastvalue.split(":")[0];
        var message = toastvalue.split(":")[1];
        if (status === "success") {
            toastr.success(message, status);
        } else if (status === "error") {
            toastr.error(message, status);
        } else if (status == "warn") {
            toastr.warning(message, status);
        }
    <?php } ?>
</script>
<script>
    var flkty = new Flickity('.main-carousel', {
        // options
        cellAlign: "left",
        freeScroll: true,
        contain: true,
        // disable previous & next buttons and dots
        prevNextButtons: false,
        pageDots: false
    });

    $('.jojo').on('shown.bs.modal', function(event) {
        $('.gallery').flickity('resize');
    });

    $('.triggerSidebar').click(function() {
        $('.sidebar-overlay').css('right', '0px')
        $('.overlay').css('display', 'block')
    })

    var sembunyikan = function() {
        $('.overlay').css('display', 'none')
        $('.sidebar-overlay').css('right', '-300px')
    }

    $('.hideSidebar').click(sembunyikan)
    $('.overlay').click(sembunyikan)
</script>
<?php if ($this->uri->segment(1) == "Aktivitas") : ?>
    <script>
        var tampung = [];

        Dropzone.options.foto = {
            url: "<?= base_url('Aktivitas/add_foto') ?>",
            autoProcessQueue: true,
            paramName: "file",
            clickable: true,
            maxFilesize: 5, //in mb
            addRemoveLinks: true,
            thumbnailWidth: null,
            thumbnailHeight: null,
            acceptedFiles: 'image/*',
            dictDefaultMessage: "<i class='ico-upload mr-2'></i> Drag & Drop File Here",
            init: function() {
                this.on("sending", function(file, xhr, formData) {
                    var name = new Date().getTime(); //or any other name you want to append with the filename
                    formData.append(name, file);
                    console.log("sending file");
                });
                this.on("success", function(file, responseText) {
                    console.log('great success');
                    tampung.push(file.upload.filename);
                    console.log(file)
                });

                this.on("addedfile", function(file) {
                    // if (this.files.length > 1) {
                    // 	this.removeFile(this.files[0]);
                    // }


                    $(this).closest('.dropzone').find('.dz-default').addClass('dz-qu')
                    var newNode = document.createElement('a');
                    // newNode.href = "#";
                    // newNode.classList = "fullscreen"
                    // newNode.innerHTML = 'Fullscreen';
                    // file.previewTemplate.appendChild(newNode);
                });
                this.on("removedfile", file => {
                    console.log("A file has been remove");
                    $('.dz-default').removeClass('dz-qu')
                });
            }
        };

        function kirimform() {
            var temp = '';

            tampung.forEach(res => {
                temp += res + '<>';
            });
            $("#gambar").val(temp)
        }
        $(document).on('click', '.fullscreen', function(e) {
            e.preventDefault();
            $(this).closest('.dropzone').find(".dz-image").css("height", 'auto');
        })
    </script>
<?php elseif ($this->uri->segment(1) == "Gudang") : ?>
    <script>
        var tampung = [];
        Dropzone.options.gudang = {
            url: "<?= base_url('Gudang/add_foto') ?>",
            autoProcessQueue: true,
            paramName: "file",
            clickable: true,
            maxFilesize: 5, //in mb
            addRemoveLinks: true,
            thumbnailWidth: null,
            thumbnailHeight: null,
            acceptedFiles: 'image/*',
            dictDefaultMessage: "<i class='ico-upload mr-2'></i> Drag & Drop File Here",
            init: function() {
                this.on("sending", function(file, xhr, formData) {
                    var name = new Date().getTime(); //or any other name you want to append with the filename
                    formData.append(name, file);
                    console.log("sending file");
                });
                this.on("success", function(file, responseText) {
                    console.log('great success');
                    tampung.push(file.upload.filename);
                    console.log(file)
                });

                this.on("addedfile", function(file) {
                    // if (this.files.length > 1) {
                    // 	this.removeFile(this.files[0]);
                    // }


                    $(this).closest('.dropzone').find('.dz-default').addClass('dz-qu')
                    var newNode = document.createElement('a');
                    // newNode.href = "#";
                    // newNode.classList = "fullscreen"
                    // newNode.innerHTML = 'Fullscreen';
                    // file.previewTemplate.appendChild(newNode);
                });
                this.on("removedfile", file => {
                    console.log("A file has been remove");
                    $('.dz-default').removeClass('dz-qu')
                });
            }
        };

        function kirimform() {
            var temp = '';

            tampung.forEach(res => {
                temp += res + '<>';
            });
            $("#gambar").val(temp)
        }
        $(document).on('click', '.fullscreen', function(e) {
            e.preventDefault();
            $(this).closest('.dropzone').find(".dz-image").css("height", 'auto');
        })
    </script>
<?php endif; ?>
</body>

</html>
