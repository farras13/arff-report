<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
	<link rel="stylesheet" href="<?= base_url(); ?>/assets/css/index.css">

    <?php if ($this->uri->segment(1) == "Dashboard" || $this->uri->segment(1) == "") : ?>
        <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <?php elseif ($this->uri->segment(1) == "Aktivitas") : ?>        
        <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/aktivitas.css" />
    <?php elseif ($this->uri->segment(1) == "Gudang") : ?>
        <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/storage.css" />
    <?php elseif ($this->uri->segment(1) == "Shipping") : ?>         
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/shipping.css" />
		
    <?php elseif ($this->uri->segment(1) == "Setting" || $this->uri->segment(1) == "Akun") : ?>
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/shipping.css" />
    <?php endif; ?>
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <title>Warehouse Rajawali Perkasa Logistic </title>

</head>

<body>
    <div class="d-sm-block d-md-none">
        <div class="overlay">a</div>
        <div class="sidebar-overlay">
            <div class="d-flex justify-content-end p-3">
                
                <img src="<?= base_url(); ?>/assets/images/icon_white_sidebar.png" width="20px" alt="" srcset="">
            </div>
            <div class="d-flex flex-column p-3" style="gap: 15px; margin-bottom: 10rem;">
                <a href="<?= base_url('Dashboard'); ?>" class="btn sidebar-btn">Dashboard</a>
                <a href="<?= base_url('Aktivitas'); ?>" class="btn sidebar-btn">Aktivitas</a>
                <a href="<?= base_url('Gudang'); ?>" class="btn sidebar-btn">Gudang</a>
                <a href="<?= base_url('Shipping'); ?>" class="btn sidebar-btn">Pengiriman</a>
            </div>
            <div class="d-flex justify-content-around">
                <a href="<?= base_url('Akun'); ?>" class="btn sidebar-btn"><img class="me-2" src="<?= base_url(); ?>/assets/images/icon_people.png" width="18px" alt="" srcset=""><b>Akun</b></a>
                <a href="<?= base_url('login/doLogout'); ?>" class="btn sidebar-btn">Keluar</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Navbar for Mobile -->
        <div class="d-sm-block d-md-none">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a href="<?= base_url('Dashboard'); ?>" class="d-flex align-items-center navbar-brand text-decoration-none">
                        <img src="<?= base_url(); ?>/assets/images/LOGO.png" width="25px" />
                        <span class="sidebar-title ms-3"><b>Rajawali Perkasa Logistic</b> | <?= $this->uri->segment(1); ?></span>
                    </a>
                    <img src="./assets/images/icon_notif.png" data-bs-toggle="modal" data-bs-target="#notif" class="ms-auto me-2 dropdown-toggle" width="20px" alt="" srcset="">
                    <a href="#" class="triggerSidebar">
                        <img src="<?= base_url(); ?>/assets/images/icon_sidebar.png" width="20px" alt="" srcset="">
                    </a>
                </div>
            </nav>
        </div>
        <div class="row flex-nowrap overflow-hidden">
            <!-- Sidebar for Dekstop -->
            <div class="d-none d-md-block col-auto col-md-3 col-xl-2 px-sm-2 px-0 sidebar" style="background-color: #39b54a;">
                <div class="d-flex flex-column align-items-center align-items-sm-start pt-3 px-sm-2 shadow-sm position-fixed" style="background-color: #39b54a; height: 100%;">
                    <a href="/" class="d-flex align-items-center align-middle pb-3 px-3 mb-md-0  text-decoration-none">
                        <div class="text-center">
                            <img class="mx-auto d-block" src="<?= base_url(); ?>/assets/images/LOGO_TOTAL.png" width="180px" />
                        </div>
                        <!-- <span class="fs-5 d-none d-lg-inline sidebar-title ms-3">ARFF Report</span> -->
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start w-100" id="menu">
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Dashboard" || $this->uri->segment(1) == "") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Dashboard'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_dashboard.png" width="20px" alt=""></i>
                                <span class="ms-2 d-none d-lg-inline">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Aktivitas") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Aktivitas'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_activity.png" width="20px" alt=""></i>
                                <span class="ms-2 d-none d-lg-inline">Activity</span>
                            </a>
                        </li>
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Gudang") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Gudang'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_storage.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-lg-inline">Storage</span>
                            </a>
                        </li>
                        <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Shipping") {
                                                    echo "active";
                                                } ?>">
                            <a href="<?= base_url('Shipping'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="<?= base_url(); ?>/assets/images/icon_shipping.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-lg-inline">Shipping</span>
                            </a>
                        </li>
						<li class="nav-item p-1 <?php if ($this->uri->segment(1) == "CCTV") {
                                                    echo "active";
                                                } ?>">
                            <a href="#" class="nav-link align-middle px-3 d-flex align-items-center">
								<i class="fa-solid fa-camera-cctv"></i>
                                <span class="ms-2 d-none d-lg-inline">CCTV</span>
                            </a>
                        </li>
                        <?php if ($this->session->userdata('role') == 1) : ?>
                            <hr class="w-100">
                            <li class="nav-item p-1 <?php if ($this->uri->segment(1) == "Setting") {
                                                        echo "active";
                                                    } ?>">
                                <a href="<?= base_url('Setting'); ?>" class="nav-link align-middle px-3 d-flex align-items-center">
                                    <i><img src="<?= base_url(); ?>/assets/images/icon_setting.png" width="20px" alt=""></i></i>
                                    <span class="ms-2 d-none d-lg-inline">Settings</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <hr />
                    <div class="d-flex justify-content-between footer-sidebar align-items-center">
                        <!-- <img src="https://github.com/mdo.png" alt="hugenerd" width="40" height="40" class="rounded-circle" /> -->
                        <div class="d-flex flex-column ms-2">
                            <span class="d-none d-lg-inline mx-1 footer-user"><?= $nama; ?></span>
                            <span class="d-none d-lg-inline mx-1 footer-nip">NIP. <?= $nip; ?></span>
                        </div>
                        <a href="<?= base_url('Login/doLogout'); ?>"><img src="<?= base_url(); ?>/assets/images/icon_logout.png" class="ms-2" width="15px" height="15px" alt=""></a>
                    </div>
                </div>
            </div>
