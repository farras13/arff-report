<div class="col py-3 mx-3">
    <h3>Detail Shipping</h3>
    <div class="p-4 card-aktifitas">
        <div class="bg-white rounded-form p-2">
            <?php foreach ($data as $d) : ?>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>NAMA BARANG</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->nama_brg; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>TANGGAL DIKIRIM</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?php if ($d->tanggal_pengiriman == "1970-01-01" || $d->tanggal_pengiriman == null || $d->tanggal_pengiriman == "0000-00-00") { ?>
                            <td>N/A</td>
                        <?php } else { ?>
                            <td><?= date('l', strtotime($d->tanggal_pengiriman)); ?>, <?= date('d F Y', strtotime($d->tanggal_pengiriman)); ?></td>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>TANGGAL DITERIMA</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?php if ($d->tanggal_diterima == "1970-01-01" || $d->tanggal_diterima == null || $d->tanggal_diterima == "0000-00-00") { ?>
                            <td>N/A</td>
                        <?php } else { ?>
                            <td><?= date('l', strtotime($d->tanggal_diterima)); ?>, <?= date('d F Y', strtotime($d->tanggal_diterima)); ?></td>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>PENGIRIM</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->name; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>TUJUAN</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?php if ($d->tujuan == "SOQ") { ?>
                            <b> INX - <?= $d->tujuan; ?> </b>
                        <?php } else { ?>
                            <b> SOQ - <?= $d->tujuan; ?> </b>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>JUMLAH</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->jumlah; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>STATUS</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-7">
                        <?= $d->status; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="width: 210px;">
                        <b>DOKUMENTASI</b>
                    </div>
                    <div class="col-2" style="width: 20px;">
                        <b>:</b>
                    </div>
                    <div class="col-12">
                        <?php foreach ($dokumentasi as $g) : ?>
                            <img src="<?= $g->image; ?>" alt="dokumentasi barang" width="510">
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>



    </div>
</div>
</div>
</div>
