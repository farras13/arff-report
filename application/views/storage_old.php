<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <!-- <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script> -->
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">


    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="./assets/css/index.css" />
    <link rel="stylesheet" href="./assets/css/storage.css" />

</head>

<body>
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-white sidebar">
                <div class="d-flex flex-column align-items-center align-items-sm-start pt-2 min-vh-100 shadow-sm">
                    <a href="/" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-decoration-none">
                        <img src="./assets/images/LOGO WEB ARFF 1.png" width="25px" />
                        <span class="fs-5 d-none d-sm-inline sidebar-title ms-3">ARFF Report</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start w-100" id="menu">
                        <li class="nav-item p-1 active">
                            <a href="/index.html" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="./assets/images/icon_dashboard.png" width="20px" alt=""></i>
                                <span class="ms-2 d-none d-sm-inline">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item p-1">
                            <a href="/activity.html" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="./assets/images/icon_activity.png" width="20px" alt=""></i>
                                <span class="ms-2 d-none d-sm-inline">Activity</span>
                            </a>
                        </li>
                        <li class="nav-item p-1">
                            <a href="/storage.html" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="./assets/images/icon_storage.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-sm-inline">Storage</span>
                            </a>
                        </li>
                        <li class="nav-item p-1">
                            <a href="/shipping.html" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="./assets/images/icon_shipping.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-sm-inline">Shipping</span>
                            </a>
                        </li>
                        <hr class="w-100">
                        <li class="nav-item p-1">
                            <a href="#" class="nav-link align-middle px-3 d-flex align-items-center">
                                <i><img src="./assets/images/icon_setting.png" width="20px" alt=""></i></i>
                                <span class="ms-2 d-none d-sm-inline">Settings</span>
                            </a>
                        </li>
                    </ul>
                    <hr />
                    <div class="d-flex justify-content-center footer-sidebar align-items-center">
                        <img src="https://github.com/mdo.png" alt="hugenerd" width="40" height="40" class="rounded-circle" />
                        <div class="d-flex flex-column ms-2">
                            <span class="d-none d-sm-inline mx-1 footer-user">Bayu</span>
                            <span class="d-none d-sm-inline mx-1 footer-nip">NIP. XXXXXXXXXXXX</span>
                        </div>
                        <img src="./assets/images/icon_logout.png" class="ms-2" width="15px" height="15px" alt="">
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col py-3">
                <h3>Gudang</h3>
                <div class="p-4 card-storage">
                    <div class="form-storage">
                        <form action="<?= base_url('Gudang/ins_data'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="bg-white rounded-form p-2">
                                <div class="dropzone" id="foto">

                                </div>
                                <input type="hidden" id="gambar" name="gambar">
                            </div>
                            <div class="card border-0 rounded-form p-2">
                                <div class="card-body">
                                    <h4>Tambah Kegiatan</h4>
                                    <div class="row g-2">
                                        <div class="col-4">
                                            <p class="m-0">Nama Barang</p>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" name="nama" id="nama" placeholder="Input Nama Barang" />
                                        </div>
                                        <div class="col-4">
                                            <p class="m-0">Jumlah Barang</p>
                                        </div>
                                        <div class="col-8">
                                            <input type="text" name="jumlah" id="jumlah" placeholder="Input Jumlah Barang" />
                                        </div>
                                        <div class="col-4">
                                            <p class="m-0">Status</p>
                                        </div>
                                        <div class="col-8">
                                            <select name="kondisi" id="kondisi">
                                                <option value="">Pilih Kondisi Barang</option>
                                                <option value="Baik">Baik</option>
                                                <option value="Rusak">Rusak</option>
                                                <option value="Kosong">Kosong</option>
                                            </select>
                                        </div>
                                        <div class="col-4"></div>
                                        <div class="col-8">
                                            <button id="submit" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="mt-4">
                    <table id="table_id" class="display table">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Status</th>
                                <th>Jumlah</th>
                                <th>Penginput</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $d) : ?>
                                <tr>
                                    <td><?= $d->nama_brg; ?></td>
                                    <td><?= $d->kondisi ?></td>
                                    <td><?= $d->stok; ?></td>
                                    <td><?= $d->created_by; ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                <img width="35px" src="./assets/images/dot_actions.png" alt="" srcset="">
                                            </button>
                                            <ul class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton1">
                                                <li class="dropdwon-item">Detail</li>
                                                <hr>
                                                <li class="dropdwon-item">Print</li>
                                                <hr>
                                                <li class="dropdwon-item">Download PDF</li>
                                                <hr>
                                                <li class="dropdwon-item" style="font-weight: 700;"><a href="<?= base_url('Gudang/upd_status/') . $d->id_gudang; ?>">Konfirmasi DIPERIKSA</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/dropzone.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table_id').DataTable();
        });
    </script>
    <script>
        var tampung = [];
        Dropzone.options.foto = {
            url: "<?= $url ?>",
            autoProcessQueue: true,
            paramName: "file",
            clickable: true,
            maxFilesize: 5, //in mb
            addRemoveLinks: true,
            thumbnailWidth: null,
            thumbnailHeight: null,
            acceptedFiles: '.png,.jpg',
            dictDefaultMessage: "<i class='ico-upload mr-2'></i> Drag & Drop File Here",
            init: function() {
                this.on("sending", function(file, xhr, formData) {
                    console.log("sending file");
                });
                this.on("success", function(file, responseText) {
                    console.log('great success');
                    tampung.push(file.upload.filename);
                    console.log(file)
                });

                this.on("addedfile", function(file) {
                    // if (this.files.length > 1) {
                    // 	this.removeFile(this.files[0]);
                    // }


                    $(this).closest('.dropzone').find('.dz-default').addClass('dz-qu')
                    var newNode = document.createElement('a');
                    newNode.href = "#";
                    newNode.classList = "fullscreen"
                    newNode.innerHTML = 'Fullscreen';
                    file.previewTemplate.appendChild(newNode);
                });
                this.on("removedfile", file => {
                    console.log("A file has been remove");
                    $('.dz-default').removeClass('dz-qu')
                });
            }
        };

        function kirimform() {
            var temp = '';

            tampung.forEach(res => {
                temp += res + '<>';
            });
            $("#gambar").val(temp)
        }
        $(document).on('click', '.fullscreen', function(e) {
            e.preventDefault();
            $(this).closest('.dropzone').find(".dz-image").css("height", 'auto');
        })
    </script>

</body>

</html>