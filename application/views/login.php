<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="https://rajawaliperkasalogistic.com/image/logo-icon.png">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="./assets/css/login.css">
    <!-- <title>Hello, world!</title> -->
</head>

<body>

    <div class="kontainer-login">
        <div class="content-login">
            <p style="font-weight: 700; margin: 0;">Rajawali Perkasa Logistik</p>
            <p style="font-weight: 400;">Warehouse Management System</p>
            <div class="text-center">
                <img src="./assets/images/LOGO.png" width="60" height="86" alt="">
            </div>
            <h1 class="fw-bold" style="font-size: 62px; margin: 0;">Masuk</h1>
            <p>Pegawai Operasional</p>
            <form class="w-75" method="POST" action="<?= base_url('Login/doLogin'); ?>">
                <div class="mb-3">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
                <div class="mb-3">
                    <input type="password" class="form-control" name="pass" placeholder="Password">
                </div>
                <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Remember Me</label>
                </div>
                <button type="submit" id="grad1">Sign in</button>
            </form>
        </div>
        <div class="text-center">
            <button onclick="keWeb()" class="btn-about">Tentang Rajawali Perkasa Logistik</button>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->

    <script>
        function keWeb() {
            window.open("https://rajawaliperkasalogistic.com/")
        }
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>

</body>

</html>
