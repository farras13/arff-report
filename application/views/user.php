<!-- Content -->
<div class="col py-3 mx-3">
    <h3 class="d-none d-md-block">User</h3>
    <div class="p-4 card-shipping">
        <form class="form-storage" action="<?= base_url('Setting/add_user'); ?>" method="POST">
            <div class="card border-0 rounded-form p-2">
                <div class="card-body">
                    <h4>Tambah User</h4>
                    <div class="row g-2">
                        <div class="col-4">
                            <p class="m-0">Nip</p>
                        </div>
                        <div class="col-8">
                            <input type="text" name="nip" id="nip" class="form-control" placeholder="Input NIP" required />
                        </div>
                        <div class="col-4">
                            <p class="m-0">Nama Lengkap</p>
                        </div>
                        <div class="col-8">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Input Nama User" required />
                        </div>
                        <div class="col-4">
                            <p class="m-0">role</p>
                        </div>
                        <div class="col-8">
                            <select name="role" id="role" class="form-control" required>
                                <option value="1">Admin</option>
                                <option value="2">Kepala Unit</option>
                                <option value="3">User</option>
                            </select>
                        </div>
                        <div class="col-4">
                            <p class="m-0">Username</p>
                        </div>
                        <div class="col-8">
                            <input type="text" name="username" id="username" class="form-control" placeholder="Input Username" required />
                        </div>
                        <div class="col-4">
                            <p class="m-0">Password</p>
                        </div>
                        <div class="col-8">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Input Password" required />
                        </div>
                        <div class="col-4"></div>
                        <div class="col-8">
                            <button type="submit" class="btn btn-success" style="border-radius: 19.5px;width: 100%;border: 0;background: #2AC670;color: #FFFFFF;font-weight: 600;padding: 4px;">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="mt-4 table-responsive">
        <table id="table_id" class="display table">
            <thead>
                <tr>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Role</th>
                    <th>Username</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $v) : ?>
                    <tr>
                        <td><?= $v->nip; ?></td>
                        <td><?= $v->name; ?></td>
                        <td><?= ($v->role == 1 ? "Admin" : $v->role == 2) ? "Kepala Unit" : "Pegawai"; ?></td>
                        <td><?= $v->username; ?></td>
                        <td><?= date('l', strtotime($v->created)); ?>, <?= date('d F Y', strtotime($v->created)); ?></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img width="35px" src="./assets/images/dot_actions.png" alt="" srcset="">
                                </button>
                                <ul class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton1">
                                    <a href="" data-bs-toggle="modal" data-bs-target="#exampleModal<?= $v->id ?>">
                                        <li class="dropdwon-item">Edit</li>
                                    </a>
                                    <hr>
                                    <li class="dropdwon-item"><a href="<?= base_url('Setting/del_user/') . $v->id ?>">Delete </a></li>
                                </ul>
                            </div>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>

<!-- Modal Tambah Pengiriman Barang -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Detail User</h4>
                <div class="row g-2">
                    <div class="col-4">
                        <p class="m-0">NIP</p>
                    </div>
                    <div class="col-8">
                        <input type="text" name="nip" id="nip" value="<?= $d->nip; ?>" />
                    </div>
                    <div class="col-4">
                        <p class="m-0">Name</p>
                    </div>
                    <div class="col-8">
                        <input type="text" name="name" id="name" value="<?= $d->name; ?>" />
                    </div>
                    <div class="col-4">
                        <p class="m-0">role</p>
                    </div>
                    <div class="col-8">
                        <select name="role" id="role" class="form-control">
                            <option value="1" <?php if ($d->role == 1) {
                                                    echo "selected";
                                                } ?>>Admin</option>
                            <option value="2" <?php if ($d->role == 2) {
                                                    echo "selected";
                                                } ?>>Kepala Unit</option>
                            <option value="3" <?php if ($d->role == 3) {
                                                    echo "selected";
                                                } ?>>User</option>
                        </select>

                    </div>
                    <div class="col-4">
                        <p class="m-0">Username</p>
                    </div>
                    <div class="col-8">
                        <input type="text" name="username" id="username" value="<?= $d->username; ?>" />
                    </div>
                    <div class="col-4">
                        <p class="m-0">Password</p>
                    </div>
                    <div class="col-8">
                        <input type="password" name="pass" id="pass" value="<?= $d->password; ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php foreach ($data as $d) : ?>
    <!-- Modal Konfirmasi Penerimaan Barang -->
    <div class="modal fade" id="exampleModal<?= $d->id; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Detail User</h4>
                    <form action="<?= base_url('Setting/upd_user/') . $d->id; ?>" method="post">
                        <div class="row g-2">
                            <div class="col-4">
                                <p class="m-0">NIP</p>
                            </div>
                            <div class="col-8">
                                <input type="text" name="nip" id="nip" value="<?= $d->nip; ?>" class="form-control" />
                            </div>
                            <div class="col-4">
                                <p class="m-0">Name</p>
                            </div>
                            <div class="col-8">
                                <input type="text" name="name" id="name" value="<?= $d->name; ?>" class="form-control" />
                            </div>
                            <div class="col-4">
                                <p class="m-0">role</p>
                            </div>
                            <div class="col-8">
                                <select name="role" id="role" class="form-control">
                                    <option value="1" <?php if ($d->role == 1) {
                                                            echo "selected";
                                                        } ?>>Admin</option>
                                    <option value="2" <?php if ($d->role == 2) {
                                                            echo "selected";
                                                        } ?>>Kepala Unit</option>
                                    <option value="3" <?php if ($d->role == 3) {
                                                            echo "selected";
                                                        } ?>>User</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <p class="m-0">Username</p>
                            </div>
                            <div class="col-8">
                                <input type="text" name="username" id="username" value="<?= $d->username; ?>" class="form-control" />
                            </div>
                            <div class="col-4">
                                <p class="m-0">Password</p>
                            </div>
                            <div class="col-8">
                                <input type="password" name="pass" id="pass" class="form-control" />
                                <small>*isilah input password jika ingin mengganti password user tersebut !</small>
                            </div>
                            <div class="col-4"></div>
                            <div class="col-8">
                                <button type="submit" class="form-control">Edit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
