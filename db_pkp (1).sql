-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Jul 2022 pada 04.37
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pkp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitas`
--

CREATE TABLE `aktivitas` (
  `id_aktivitas` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `pembuat` int(11) NOT NULL,
  `is_seen` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `aktivitas`
--

INSERT INTO `aktivitas` (`id_aktivitas`, `judul`, `tanggal`, `status`, `deskripsi`, `pembuat`, `is_seen`, `is_deleted`) VALUES
(1, 'Peresmian ', '2022-07-08', 1, 'qweer', 1, 0, 0),
(2, 'membuat asmr', '2022-07-08', 0, 'rtyuio', 1, 0, 0),
(4, 'Gebyar Asoy ceria', '2022-07-10', 1, 'asdasdas', 1, 1, 0),
(5, 'Gebyar LA 2022', '2022-07-10', 0, 'asdasdasd', 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitas_galeri`
--

CREATE TABLE `aktivitas_galeri` (
  `id` int(11) NOT NULL,
  `aktivitas_id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `aktivitas_galeri`
--

INSERT INTO `aktivitas_galeri` (`id`, `aktivitas_id`, `image`) VALUES
(1, 1, 'http://localhost/abw/assets/upload/LOGO_WEB_ARFF_1.png'),
(2, 1, 'http://localhost/abw/assets/upload/icon_storage.png'),
(3, 2, 'http://localhost/abw/assets/upload/icon_shipping.png'),
(4, 2, 'http://localhost/abw/assets/upload/icon_setting.png'),
(5, 4, 'http://localhost/abw/assets/upload/7a94a2fb-0c3c-4445-ad0b-a96ee6ce16cc_1.png'),
(6, 4, 'http://localhost/abw/assets/upload/9f638719-5c15-44ca-884c-e9720da57278_1.png'),
(7, 4, 'http://localhost/abw/assets/upload/c01a554f8d91490e6767058a8b463003c81e1777.jpg'),
(8, 5, 'http://localhost/abw/assets/upload/Screenshot_2020-09-23_080449.png'),
(9, 5, 'http://localhost/abw/assets/upload/Screenshot_2020-09-28_210939.png'),
(10, 5, 'http://localhost/abw/assets/upload/Screenshot_2021-08-13_193413.png'),
(11, 5, 'http://localhost/abw/assets/upload/Screenshot_2021-08-16_065325.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gudang`
--

CREATE TABLE `gudang` (
  `id_gudang` int(11) NOT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `modified` date DEFAULT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gudang`
--

INSERT INTO `gudang` (`id_gudang`, `nama_brg`, `stok`, `kondisi`, `created_by`, `created`, `modified`, `is_deleted`) VALUES
(1, 'Farras', 1, 'Rusak', '1', '2022-07-09', '2022-07-09', 0),
(2, 'BAarang askdl', 15, 'Baik', '1', '2022-07-09', '2022-07-09', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gudang_gallery`
--

CREATE TABLE `gudang_gallery` (
  `id_gg` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gudang_gallery`
--

INSERT INTO `gudang_gallery` (`id_gg`, `gudang_id`, `image`, `created`) VALUES
(1, 1, 'http://localhost/abw/assets/upload/icon_setting.png', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `shiping`
--

CREATE TABLE `shiping` (
  `id_shiping` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `tanggal_pengiriman` date NOT NULL,
  `tanggal_diterima` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `shiping`
--

INSERT INTO `shiping` (`id_shiping`, `gudang_id`, `jumlah`, `tujuan`, `tanggal_pengiriman`, `tanggal_diterima`, `status`, `pengirim`, `is_deleted`) VALUES
(1, 0, 14, 'INX', '2022-07-09', '2022-07-10', 'Diterima', 1, 0),
(2, 0, 13, 'SOQ', '2022-07-09', '0000-00-00', 'Dikirim', 1, 0),
(3, 0, 15, 'INX', '2022-07-09', '0000-00-00', 'Dikirim', 1, 0),
(4, 0, 15, 'INX', '2022-07-09', '0000-00-00', 'Dikirim', 1, 0),
(5, 0, 15, 'INX', '2022-07-09', '0000-00-00', 'Dikirim', 1, 0),
(6, 0, 15, 'INX', '2022-07-09', '0000-00-00', 'Dikirim', 1, 0),
(7, 2, 15, 'INX', '2022-07-09', '2022-07-10', 'Diterima', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created` date NOT NULL,
  `role` int(11) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gambar` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `created`, `role`, `nip`, `name`, `gambar`, `is_active`, `is_deleted`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2022-07-05', 1, '1931710129', 'Admin', '', 1, 0),
(2, 'kepala', '870f669e4bbbfa8a6fde65549826d1c4', '2022-07-04', 2, '1931710131', 'Nasar', '', 1, 0),
(3, 'pegawai', '047aeeb234644b9e2d4138ed3bc7976a', '2022-07-04', 3, '2031910129', 'Wai', '', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD PRIMARY KEY (`id_aktivitas`);

--
-- Indeks untuk tabel `aktivitas_galeri`
--
ALTER TABLE `aktivitas_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id_gudang`);

--
-- Indeks untuk tabel `gudang_gallery`
--
ALTER TABLE `gudang_gallery`
  ADD PRIMARY KEY (`id_gg`);

--
-- Indeks untuk tabel `shiping`
--
ALTER TABLE `shiping`
  ADD PRIMARY KEY (`id_shiping`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `aktivitas`
--
ALTER TABLE `aktivitas`
  MODIFY `id_aktivitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `aktivitas_galeri`
--
ALTER TABLE `aktivitas_galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id_gudang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `gudang_gallery`
--
ALTER TABLE `gudang_gallery`
  MODIFY `id_gg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `shiping`
--
ALTER TABLE `shiping`
  MODIFY `id_shiping` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
