-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 09, 2022 at 09:12 PM
-- Server version: 5.7.38
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inanwata_pkp`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas`
--

CREATE TABLE `aktivitas` (
  `id_aktivitas` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `pembuat` int(11) NOT NULL,
  `is_seen` int(11) NOT NULL,
  `image` text NOT NULL,
  `pengunggah` varchar(30) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aktivitas`
--

INSERT INTO `aktivitas` (`id_aktivitas`, `judul`, `tanggal`, `status`, `deskripsi`, `pembuat`, `is_seen`, `image`, `pengunggah`, `is_deleted`) VALUES
(1, 'Peresmian ', '2022-07-09', 1, 'Peresmian di tanah jawa', 1, 0, '', '', 0),
(2, 'Workshop Internal', '2022-07-09', 1, 'Pengadaan workshop untuk masa depan pegawai', 1, 1, '', '', 1),
(3, 'Babat Rumput', '2022-07-09', 1, 'babat rumput daerah runway dengan mower', 1, 1, '', '', 0),
(4, 'pembukaan', '2022-07-07', 1, 'pembukaan latsar kemenhub', 1, 1, '', '', 0),
(5, 'service kendaraan PKP', '2022-07-01', 1, 'service kendaraan pkp pk', 1, 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas_galeri`
--

CREATE TABLE `aktivitas_galeri` (
  `id` int(11) NOT NULL,
  `aktivitas_id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aktivitas_galeri`
--

INSERT INTO `aktivitas_galeri` (`id`, `aktivitas_id`, `image`) VALUES
(1, 1, 'https://inanwatan.com/assets/upload/LOGO_WEB_ARFF_1.png'),
(2, 1, 'https://inanwatan.com/assets/upload/icon_storage.png'),
(3, 2, 'https://inanwatan.com/assets/upload/icon_logout.png'),
(4, 2, 'https://inanwatan.com/assets/upload/icon_setting.png'),
(5, 3, 'https://inanwatan.com/assets/upload/eda2952b-ac98-43bb-8f69-c4bda036d5b8.JPG'),
(6, 3, 'https://inanwatan.com/assets/upload/baground.jpg'),
(7, 3, 'https://inanwatan.com/assets/upload/sky.jpg'),
(8, 4, 'https://inanwatan.com/assets/upload/baground.jpg'),
(9, 5, 'https://inanwatan.com/assets/upload/ae5fadbf-8350-48f5-a8e0-777d9a2ce34c.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id_gudang` int(11) NOT NULL,
  `nama_brg` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created` date NOT NULL,
  `modified` date DEFAULT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id_gudang`, `nama_brg`, `stok`, `kondisi`, `created_by`, `created`, `modified`, `is_deleted`) VALUES
(1, 'Apar', 3, 'Kosong', '1', '2022-07-09', '2022-07-09', 0),
(2, 'Brush Cutter', 2, 'Baik', '1', '2022-07-09', '2022-07-09', 0),
(3, 'Bama', 1, 'Baik', '1', '2022-07-09', '2022-07-09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gudang_gallery`
--

CREATE TABLE `gudang_gallery` (
  `id_gg` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gudang_gallery`
--

INSERT INTO `gudang_gallery` (`id_gg`, `gudang_id`, `image`, `created`) VALUES
(1, 1, 'https://inanwatan.com/assets/upload/9f638719-5c15-44ca-884c-e9720da57278_1.png', 0),
(2, 2, 'https://inanwatan.com/assets/upload/baground.jpg', 0),
(3, 2, 'https://inanwatan.com/assets/upload/sky.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shiping`
--

CREATE TABLE `shiping` (
  `id_shiping` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `tanggal_pengiriman` date NOT NULL,
  `tanggal_diterima` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shiping`
--

INSERT INTO `shiping` (`id_shiping`, `gudang_id`, `jumlah`, `tujuan`, `tanggal_pengiriman`, `tanggal_diterima`, `status`, `pengirim`, `is_deleted`) VALUES
(1, 1, 10, 'SOQ', '2022-07-09', '2022-07-09', 'Diterima', 1, 1),
(2, 0, 2, 'INX', '2022-07-09', '2022-07-09', 'Diterima', 1, 1),
(3, 0, 2, 'INX', '2022-07-09', '2022-07-09', 'Diterima', 1, 1),
(4, 3, 1, 'INX', '2022-07-09', '2022-07-09', 'Diterima', 1, 0),
(5, 1, 2, 'SOQ', '2022-07-07', '2022-07-09', 'Diterima', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created` date NOT NULL,
  `role` int(11) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gambar` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `created`, `role`, `nip`, `name`, `gambar`, `is_active`, `is_deleted`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2022-07-03', 1, '-', 'Admin', '', 1, 0),
(2, 'nasrun', '023c5eae3fc5d702880eb86fda84b77b', '2022-07-03', 2, '198409212010121 001\r\n', 'Nasrun', '', 1, 0),
(3, 'bayu', 'a430e06de5ce438d499c2e4063d60fd6', '2022-07-03', 3, '200004042021121002', 'Agung Bayu', '', 1, 0),
(4, 'yopie', '1750a1dc561d4bb342321ec74a445389', '2022-07-03', 3, '198508302010121003', 'Yopie Klemens Swabra', '', 1, 0),
(5, 'nizar', 'bf93e5f003e40c3fba44e512aecd3cdc', '2022-07-03', 3, '199809102018011001', 'Nizar Rahmatul Lukman', '', 1, 0),
(6, 'sudirman', '085fa40f2b5653095f3b91e6cc26769d', '2022-07-03', 3, '-', 'Sudirman', '', 1, 0),
(7, 'brando', '16132382c21299b781dfaf024b541e4d', '2022-07-03', 3, '-', 'Brando Fatari', '', 1, 0),
(8, 'luis', '502ff82f7f1f8218dd41201fe4353687', '2022-07-03', 3, '-', 'Luis D. Gobotuga', '', 1, 0),
(9, 'antonius', '5a60668bacacd99584dcc0ab99893e7f', '2022-07-09', 3, '-', 'Antonius Wiripae', '', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD PRIMARY KEY (`id_aktivitas`);

--
-- Indexes for table `aktivitas_galeri`
--
ALTER TABLE `aktivitas_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id_gudang`);

--
-- Indexes for table `gudang_gallery`
--
ALTER TABLE `gudang_gallery`
  ADD PRIMARY KEY (`id_gg`);

--
-- Indexes for table `shiping`
--
ALTER TABLE `shiping`
  ADD PRIMARY KEY (`id_shiping`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktivitas`
--
ALTER TABLE `aktivitas`
  MODIFY `id_aktivitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `aktivitas_galeri`
--
ALTER TABLE `aktivitas_galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id_gudang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gudang_gallery`
--
ALTER TABLE `gudang_gallery`
  MODIFY `id_gg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shiping`
--
ALTER TABLE `shiping`
  MODIFY `id_shiping` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
